/* v1.1.1 */
Layout:
- change the flag marker

Automation:
- create the AppStore delivery file with Fastlane



/* v1.1.2 */
Layout:
- During search, when there are less than 3 characters, they should be red; when there are more than 3 characters, they should be green
- During serach, if the user doesn't write more than 3 characters for one second or so, the app should show a hint;



/* Later */
- add more interactive animations
- improve the autocomplete/search:
	- https://developer.apple.com/library/ios/documentation/CoreLocation/Reference/CLGeocoder_class/index.html)
	- iOS 9.3 only: http://stackoverflow.com/questions/33380711/how-to-implement-auto-complete-for-address-using-apple-map-kit
- more things to show on the map:
	- https://cyclestreets.net/api/v2/photomap.categories/
- animate marker removal
- show destination point based on route, not on the last selected marker (maybe add the original end point and starting point in the route object)


/* Notes */
appstore delivery file:
- version increase "- version bump, release-1.0.1"
- com.citybiker.ios

/* Ideas */
- when the route details is shown, if the user taps on the addres -> animate to address locations