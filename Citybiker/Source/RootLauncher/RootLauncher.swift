//
//  RootLauncher.swift
//  Citybiker
//
//  Created by Vlad on 8/8/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

let kAppLaunched = "APP_LAUNCHED"

class RootLauncher {
    
    private let appDelegate: AppDelegate
    
    
    // MARK: - Initializers
    
    init(appDelegate: AppDelegate) {
        self.appDelegate = appDelegate
    }
    
    
    
    // MARK: - Public Methods
    
    func launch() {
        let rootController = appLaunched() ? mapController : introController;
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = rootController()
        window.makeKeyAndVisible()
        
        appDelegate.window = window
    }
    
    
    
    // MARK: - Private Methods
    
    private func appLaunched() -> Bool {
        let userDefaults = UserDefaults.standard
        return userDefaults.object(forKey: kAppLaunched) == nil ? false : true
    }
    
    private func introController() -> UIViewController {
        return UIStoryboard(name: "Intro", bundle: nil).instantiateInitialViewController()!
    }
    
    private func mapController() -> UIViewController {
        return UIStoryboard(name: "Map", bundle: nil).instantiateInitialViewController()!
    }
    
}
