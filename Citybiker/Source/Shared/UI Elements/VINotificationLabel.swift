//
//  NotificationLabel.swift
//  EverestApps
//
//  Created by Vlad on 7/6/16.
//  Copyright © 2016 EverestApps. All rights reserved.
//

import UIKit

extension RangeReplaceableCollection where Iterator.Element : Equatable {
    mutating func remove(object : Iterator.Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
}


@IBDesignable
class NotificationLabel: UILabel {
    
    static private var allNotificationWindows: [UIWindow] = []
    static private let defaultTextColor =  UIColor.rgbColor(red: 66, green: 66, blue: 66, alpha: 1)
    
    // MARK: Notification Label properties
    
    static private let minimalNotificationHeight: CGFloat = 16.0
    static private let notificationHeightOffset: CGFloat = 5.0
    static var notificationFont: UIFont {
        return UIFont(name: "Montserrat-Regular", size: 9)!
    }
    
    // MARK: Animation timing
    
    static let fadeInDuration: TimeInterval = 0.2
    static let fadeOutDuration: TimeInterval = 0.3
    static let readingCharactersPerSecond = 20.0
    static let defaultAnimationDuration: TimeInterval = 2.0
    
    
    
    // MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        init_NotificationLabel()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        init_NotificationLabel()
    }
    
    private final func init_NotificationLabel() {
        numberOfLines = 0
        textAlignment = .center
        font = UIFont(name: "Montserrat-Regular", size: 9)
        textColor = type(of: self).defaultTextColor
        backgroundColor = UIColor.white
        frame = CGRect(x: 0,
                       y: 0,
                       width: UIScreen.main.bounds.size.width,
                       height: type(of: self).minimalNotificationHeight)
    }
    
    
    
    // MARK: - Public Methods
    
    static func showNotification(text: String) {
        showNotification(text: text, duration: nil)
    }
    
    static func showNotification(text: String, duration: TimeInterval?) {
        if allNotificationWindows.count > 0 {
            hideAllWindows()
        }
        
        let labelFrame = self.labelFrame(for: text)
        let windowFrame = CGRect(x: 0, y: -labelFrame.height, width: labelFrame.width, height: labelFrame.height)
        
        let notificationWindow = createNotificationWindow(frame: windowFrame)
        
        let notificationLabel = createNotificationLabel(text: text, frame: labelFrame)
        notificationWindow.addSubview(notificationLabel)
        
        let animationDuration = duration == nil ? max(Double(text.characters.count) / readingCharactersPerSecond, defaultAnimationDuration): duration
        
        allNotificationWindows.append(notificationWindow)
        animate(windowToAnimate: notificationWindow, duration: animationDuration!)
    }
    
    
    
    // MARK: - Private Methods
    
    static private func sizeFor(text: String, font: UIFont, maxSize: CGSize) -> CGSize {
        let attributedString = NSAttributedString.init(string: text, attributes: [NSFontAttributeName:font])
        let rect = attributedString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let size = CGSize(width: rect.size.width, height: rect.size.height)
        
        return size
    }
    
    static private func labelFrame(for text: String) -> CGRect {
        let labelSize = sizeFor(text: text,
                                font: notificationFont,
                                maxSize: CGSize(width: UIScreen.main.bounds.width , height: 999))
        let labelHeight = max(labelSize.height + notificationHeightOffset,
                              minimalNotificationHeight)
        
        return CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: labelHeight)
    }
    
    static private func createNotificationWindow(frame: CGRect) -> UIWindow {
        let window = UIWindow(frame: frame)
        window.backgroundColor = UIColor.clear
        window.windowLevel = UIWindowLevelStatusBar
        
        return window
    }
    
    static private func createNotificationLabel(text: String, frame: CGRect) -> NotificationLabel {
        let notificationLabel = NotificationLabel()
        notificationLabel.text = text
        notificationLabel.frame = frame
        
        return notificationLabel
    }
    
    static private func animate(windowToAnimate: UIWindow, duration: TimeInterval) {
        let originalWindowFrame = windowToAnimate.frame
        var movedWindowFrame = originalWindowFrame
        movedWindowFrame.origin.y += originalWindowFrame.size.height
        
        windowToAnimate.isHidden = false
        
        UIView.animate(withDuration: fadeInDuration, delay: 0, options: .curveEaseOut, animations: {
            windowToAnimate.frame = movedWindowFrame
        })
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            UIView.animate(withDuration: fadeOutDuration, delay: 0, options: .curveEaseOut, animations: {
                windowToAnimate.frame = originalWindowFrame
            }) { success in
                windowToAnimate.isHidden = true
                allNotificationWindows.remove(object: windowToAnimate)
            }
        }
    }
    
    static private func hideAllWindows() {
        for window in allNotificationWindows {
            var targetFrame = window.frame
            targetFrame.origin.y = -targetFrame.size.height
            
            UIView.animate(withDuration: fadeOutDuration, delay: 0, options: .curveEaseOut, animations: {
                window.frame = targetFrame
            }) { success in
                window.isHidden = true
                allNotificationWindows.remove(object: window)
            }
        }
    }
    
}
