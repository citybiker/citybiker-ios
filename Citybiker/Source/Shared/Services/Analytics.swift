//
//  Analytics.swift
//  Citybiker
//
//  Created by Vlad on 9/15/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import Firebase

public struct Analytics {
    
    static private let kAppVersion = "app_version"
    static private let kButtonName = "button_name"
    
    static func appVersion() -> String {
        return Bundle.main.infoDictionary!["CFBundleVersion"] as! String
    }
    
    
    
    // MARK: - App Level
    
    static func logAppLaunchEvent() {
        FIRAnalytics.logEvent(withName: kFIREventAppOpen, parameters:
            [
                kAppVersion : self.appVersion() as NSObject
            ]
        )
    }
    
    static func logAppEnterForegroundEvent() {
        FIRAnalytics.logEvent(withName: "app_enter_foreground", parameters:
            [
                kAppVersion : self.appVersion() as NSObject
            ]
        )
    }
    
    static func logAppEnterBackgroundEvent() {
        FIRAnalytics.logEvent(withName: "app_enter_background", parameters:
            [
                kAppVersion : self.appVersion() as NSObject
            ]
        )
    }
    
    
    
    // MARK: - Intro
    
    static func logTutorialBeginEvent() {
        FIRAnalytics.logEvent(withName: kFIREventTutorialBegin, parameters:
            [
                kAppVersion : self.appVersion() as NSObject
            ]
        )
    }
    
    static func logTutorialCompleteEvent() {
        FIRAnalytics.logEvent(withName: kFIREventTutorialComplete, parameters:
            [
                kAppVersion : self.appVersion() as NSObject
            ]
        )
    }
    
    
    
    // MARK: - Map
    
    static func logMyLocationButtonTappedEvent() {
        FIRAnalytics.logEvent(withName: kFIREventViewItem, parameters:
            [
                kAppVersion : self.appVersion() as NSObject,
                kButtonName : "My Location Button" as NSObject
            ]
        )
    }
    
    static func logSearchButtonTappedEvent() {
        FIRAnalytics.logEvent(withName: kFIREventViewItem, parameters:
            [
                kAppVersion : self.appVersion() as NSObject,
                kButtonName : "Search Button" as NSObject
            ]
        )
    }
    
    static func logDismissSearchButtonTappedEvent() {
        FIRAnalytics.logEvent(withName: kFIREventViewItem, parameters:
            [
                kAppVersion : self.appVersion() as NSObject,
                kButtonName : "Dismiss Search Button" as NSObject
            ]
        )
    }
    
    static func logDismissKeyboardButtonTappedEvent() {
        FIRAnalytics.logEvent(withName: kFIREventViewItem, parameters:
            [
                kAppVersion : self.appVersion() as NSObject,
                kButtonName : "Dismiss Keyboard Button" as NSObject
            ]
        )
    }
    
    static func logDestinationButtonTappedEvent() {
        FIRAnalytics.logEvent(withName: kFIREventViewItem, parameters:
            [
                kAppVersion : self.appVersion() as NSObject,
                kButtonName : "Destination Button" as NSObject
            ]
        )
    }
    
    static func logDismissDestinationButtonTappedEvent() {
        FIRAnalytics.logEvent(withName: kFIREventViewItem, parameters:
            [
                kAppVersion : self.appVersion() as NSObject,
                kButtonName : "Dismiss Destination Button" as NSObject
            ]
        )
    }
    
    static func logSearchResultsShownEvent() {
        FIRAnalytics.logEvent(withName: kFIREventSearch, parameters:
            [
                kAppVersion : self.appVersion() as NSObject
            ]
        )
    }
    
    static func logSelectedDestinationEvent() {
        FIRAnalytics.logEvent(withName: kFIRParameterDestination, parameters:
            [
                kAppVersion : self.appVersion() as NSObject
            ]
        )
    }
    
}
