//
//  CitybikerErrors.swift
//  Citybiker
//
//  Created by Vlad on 8/13/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

struct CitybikerError {
    
    static let descriptionKey = "CitybikerErrorDescriptionKey"
    
    func networkGenericError(domain: String, statusCode: NSInteger) -> NSError {
        let generalError = NSLocalizedString("GENERAL_ERROR", comment: "General error")
        let errorDescription = generalError + " (\(statusCode))"
        
        return NSError(domain: domain,
                       code: statusCode,
                       userInfo: [type(of: self).descriptionKey : errorDescription])
    }
    
}
