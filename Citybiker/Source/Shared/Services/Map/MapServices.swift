//
//  MapServices.swift
//  Citybiker
//
//  Created by Vlad on 8/5/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import MapKit

/*
 Northest and Southest points are calculated based on latitude
 Western and Eastern points are calculated based on longitude
 */

struct PlaceExtremePoints {
    
    let northernmostPoint: CLLocationCoordinate2D
    let southernmostPoint: CLLocationCoordinate2D
    let westernmostPoint: CLLocationCoordinate2D
    let easternmostPoint: CLLocationCoordinate2D
    
}

struct PlaceBounds {
    
    let northWest: CLLocationCoordinate2D
    let northEast: CLLocationCoordinate2D
    let southWest: CLLocationCoordinate2D
    let southEast: CLLocationCoordinate2D
    
    init(extremePoints: PlaceExtremePoints) {
        northWest = CLLocationCoordinate2D(latitude: extremePoints.northernmostPoint.latitude,
                                           longitude: extremePoints.westernmostPoint.longitude)
        northEast = CLLocationCoordinate2D(latitude: extremePoints.northernmostPoint.latitude,
                                           longitude: extremePoints.easternmostPoint.longitude)
        southWest = CLLocationCoordinate2D(latitude: extremePoints.southernmostPoint.latitude,
                                           longitude: extremePoints.westernmostPoint.longitude)
        southEast = CLLocationCoordinate2D(latitude: extremePoints.southernmostPoint.latitude,
                                           longitude: extremePoints.easternmostPoint.longitude)
    }
    
    init(north: Double, east: Double, south: Double, west: Double) {
        northWest = CLLocationCoordinate2D(latitude: north, longitude: west)
        northEast = CLLocationCoordinate2D(latitude: north, longitude: east)
        southWest = CLLocationCoordinate2D(latitude: south, longitude: west)
        southEast = CLLocationCoordinate2D(latitude: south, longitude: east)
    }
    
}


class MapServices {
    
    private static let northernmostPoint = CLLocationCoordinate2D(latitude: 60.85,
                                                                  longitude: -0.8666666666666667)
    private static let southernmostPoint = CLLocationCoordinate2D(latitude: 49.85,
                                                                  longitude: -6.4)
    private static let westernmostPoint = CLLocationCoordinate2D(latitude: 57.583333333333336,
                                                                 longitude: -13.683333333333334)
    private static let easternmostPoint = CLLocationCoordinate2D(latitude: 52.483333333333334,
                                                                 longitude: 1.7666666666666666)
    
    static func unitedKingdomExtremePoints() -> PlaceExtremePoints {
        return PlaceExtremePoints(northernmostPoint: MapServices.northernmostPoint,
                                  southernmostPoint: MapServices.southernmostPoint,
                                  westernmostPoint: MapServices.westernmostPoint,
                                  easternmostPoint: MapServices.easternmostPoint)
    }
    
    static func unitedKingdomBounds() -> PlaceBounds {
        return PlaceBounds(extremePoints: unitedKingdomExtremePoints())
    }
    
    static func unitedKingdomRegion() -> MKCoordinateRegion {
        let uk = unitedKingdomBounds()
        
        let latitudeDelta = uk.northEast.latitude - uk.southWest.latitude
        var longitudeDelta = uk.northEast.longitude - uk.southWest.longitude
        var center = CLLocationCoordinate2D(latitude: (uk.southWest.latitude + uk.northEast.latitude) / 2,
                                            longitude: (uk.southWest.longitude + uk.northEast.longitude) / 2)
        
        if longitudeDelta < 0 {
            longitudeDelta += 360
            center.longitude += 180
        }
        
        let coordinateSpan = MKCoordinateSpan(latitudeDelta: latitudeDelta,
                                              longitudeDelta: longitudeDelta)
        return MKCoordinateRegion(center: center, span: coordinateSpan)
    }
    
}
