//
//  NetworkSharedServices.swift
//  Citybiker
//
//  Created by Vlad on 8/6/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

public typealias JSONDictionary = [String: Any]
