//
//  CBColors.swift
//  Citybiker
//
//  Created by Vlad on 9/9/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

import Foundation
import UIKit

public extension UIColor {
    
    static func cbLightGrayColor() -> UIColor {
        return UIColor.rgbColorWithWhite(white: 227, alpha: 1)
    }
    
    static func cbOrangeColor() -> UIColor {
        return UIColor.rgbColor(red: 231, green: 81, blue: 0, alpha: 1)
    }
        
}
