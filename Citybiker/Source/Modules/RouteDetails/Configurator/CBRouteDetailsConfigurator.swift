//
//  CBRouteDetailsCBRouteDetailsConfigurator.swift
//  Citybiker
//
//  Created by Vlad Ionita on 12/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class RouteDetailsModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? RouteDetailsViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: RouteDetailsViewController) {
        let router = RouteDetailsRouter()
        
        let presenter = RouteDetailsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = RouteDetailsInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
