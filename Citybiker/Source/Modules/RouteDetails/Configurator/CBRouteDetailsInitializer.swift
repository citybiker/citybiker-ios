//
//  CBRouteDetailsCBRouteDetailsInitializer.swift
//  Citybiker
//
//  Created by Vlad Ionita on 12/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class RouteDetailsModuleInitializer: NSObject {
    
    @IBOutlet weak var routeDetailsViewController: RouteDetailsViewController!
    
    override func awakeFromNib() {
        let configurator = RouteDetailsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: routeDetailsViewController)
    }
    
}
