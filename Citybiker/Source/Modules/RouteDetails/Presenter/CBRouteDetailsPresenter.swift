//
//  RouteDetailsCBRouteDetailsPresenter.swift
//  Citybiker
//
//  Created by Vlad Ionita on 12/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

class RouteDetailsPresenter: RouteDetailsModuleInput, RouteDetailsViewOutput, RouteDetailsInteractorOutput {
    
    static private let kSecondsPerMinute: Double = 60
    static private let kMetersPerKilometer: Double = 1000
    static private let kKilometersPerMile: Double = 0.62137
    
    weak var view: RouteDetailsViewInput!
    var interactor: RouteDetailsInteractorInput!
    var router: RouteDetailsRouterInput!
    
    private var output: RouteDetailsModuleOutput?
    private var routeIsShown: Bool = false
    
    
    // MARK: - RouteDetailsModuleInput
    
    func configureModule(delegate: RouteDetailsModuleOutput) {
        output = delegate
    }
    
    func setRouteDetails(address: String, time: Double, length: Double) {
        view.set(routeAddress: address)
        let timeStringResult = timeString(from: time)
        view.set(routeTime: timeStringResult.timeString, isSingularTimeUnit: timeStringResult.isSingular)
        view.set(routeLength: lengthString(from: length))
        
        if !routeIsShown {
            routeIsShown = !routeIsShown
            view.showRouteView()
        }
    }
    
    
    
    // MARK: - RouteDetailsViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
    }
    
    func didTapRefreshButton() {
        output?.userDidTapRefresh()
    }
    
    func didTapCloseButton() {
        view.hideRouteView()
        output?.userDidTapClose()
    }
    
    
    
    // MARK: - Private Methods
    
    private func timeString(from time: Double) -> (timeString: String, isSingular: Bool) {
        let timeInMinutes = time / type(of: self).kSecondsPerMinute
        let roundedTimeInMinutes = ceil(timeInMinutes)
        
        return (String(format: "%.0f", arguments: [roundedTimeInMinutes]), roundedTimeInMinutes == 1)
    }
    
    private func lengthString(from length: Double) -> String {
        let lengthInKilometers = length / type(of: self).kMetersPerKilometer
        let lengthInMiles = lengthInKilometers * type(of: self).kKilometersPerMile
        
        return String(format: "%.1f", arguments: [lengthInMiles])
    }
    
}
