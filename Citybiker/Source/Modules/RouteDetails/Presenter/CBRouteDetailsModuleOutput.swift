//
//  CBRouteDetailsModuleOutput.swift
//  Citybiker
//
//  Created by Vlad on 10/12/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

protocol RouteDetailsModuleOutput: class {
    
    func userDidTapRefresh()
    func userDidTapClose()
    
}
