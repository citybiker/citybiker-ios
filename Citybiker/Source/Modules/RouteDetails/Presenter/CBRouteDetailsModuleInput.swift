//
//  RouteDetailsCBRouteDetailsModuleInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 12/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

protocol RouteDetailsModuleInput: class {
    
    func configureModule(delegate: RouteDetailsModuleOutput)
    func setRouteDetails(address: String, time: Double, length: Double)
    
}
