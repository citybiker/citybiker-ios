//
//  RouteDetailsCBRouteDetailsViewInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 12/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

protocol RouteDetailsViewInput: class {
    
    func setupInitialState()
    
    func showRouteView()
    func hideRouteView()
    
    func set(routeAddress: String)
    func set(routeTime: String, isSingularTimeUnit: Bool)
    func set(routeLength: String)
    
}
