//
//  RouteDetailsCBRouteDetailsViewController.swift
//  Citybiker
//
//  Created by Vlad Ionita on 12/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import pop

class RouteDetailsViewController: UIViewController, RouteDetailsViewInput {
    
    var output: RouteDetailsViewOutput!
    
    @IBOutlet weak private var routeDetailsView: UIView!
    @IBOutlet weak private var addressLabel: UILabel!
    @IBOutlet weak private var milesLabel: UILabel!
    @IBOutlet weak private var milesDescriptionLabel: UILabel!
    @IBOutlet weak private var timeLabel: UILabel!
    @IBOutlet weak private var timeDescriptionLabel: UILabel!
    @IBOutlet weak private var routeDetailsViewTop: NSLayoutConstraint!
    static private let hidenRouteDetailsViewTopConstraint = -100
    static private let shownRouteDetailsViewTopConstraint = 27.0
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - RouteDetailsViewInput
    
    func setupInitialState() {
        localizeScreen()
    }
    
    func showRouteView() {
        let springAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant)
        springAnimation?.toValue = type(of: self).shownRouteDetailsViewTopConstraint
        springAnimation?.springBounciness = 10
        springAnimation?.springSpeed = 8
        routeDetailsViewTop.pop_add(springAnimation, forKey: "showRouteDetails")
    }
    
    func hideRouteView() {
        let springAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant)
        springAnimation?.toValue = type(of: self).hidenRouteDetailsViewTopConstraint
        springAnimation?.springBounciness = 10
        springAnimation?.springSpeed = 8
        routeDetailsViewTop.pop_add(springAnimation, forKey: "hidegRouteDetails")
    }
    
    func set(routeAddress: String) {
        addressLabel.text = routeAddress
    }
    
    func set(routeTime: String, isSingularTimeUnit: Bool) {
        timeLabel.text = routeTime
        
        let timeDescriptionText: String
        if isSingularTimeUnit {
            timeDescriptionText = NSLocalizedString("ROUTE_DETAILS_TIME_DESCRIPTION_SINGULAR",
                                                    comment: "Singular - minute")
        } else {
            timeDescriptionText = NSLocalizedString("ROUTE_DETAILS_TIME_DESCRIPTION_PLURAL",
                                                    comment: "Plural - minute")
        }
        timeDescriptionLabel.text = timeDescriptionText
    }
    
    func set(routeLength: String) {
        milesLabel.text = routeLength
    }
    
    
    
    // MARK: - Actions
    
    @IBAction internal func refreshButtonTapped() {
        output.didTapRefreshButton()
    }
    
    @IBAction internal func closeButtonTapped() {
        output.didTapCloseButton()
    }
    
    
    
    // MARK: - Private Methods
    
    private func localizeScreen() {
        func localizeTimeDescriptionLabel() {
            let timeDescriptionText = NSLocalizedString("ROUTE_DETAILS_TIME_DESCRIPTION", comment: "Time")
            timeDescriptionLabel.text = timeDescriptionText
        }
        
        func localizeMilesDescriptionLabel() {
            let milesDescriptionText = NSLocalizedString("ROUTE_DETAILS_MILES_DESCRIPTION", comment: "Miles")
            milesDescriptionLabel.text = milesDescriptionText
        }
        
        localizeTimeDescriptionLabel()
        localizeMilesDescriptionLabel()
    }
    
}
