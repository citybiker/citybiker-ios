//
//  RouteDetailsCBRouteDetailsViewOutput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 12/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

protocol RouteDetailsViewOutput {
    
    func viewIsReady()
    
    func didTapRefreshButton()
    func didTapCloseButton()
    
}
