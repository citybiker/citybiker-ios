//
//  CBSearchCBSearchInitializer.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class SearchModuleInitializer: NSObject {
    
    @IBOutlet weak var searchViewController: SearchViewController!
    
    override func awakeFromNib() {
        let configurator = SearchModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: searchViewController)
    }
    
}
