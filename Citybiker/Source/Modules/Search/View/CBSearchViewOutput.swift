//
//  SearchCBSearchViewOutput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

protocol SearchViewOutput {
    
    func viewIsReady()
    
    func didChangeSearchText(text: String)
    func didSelectSearchResult(index: Int)
    
}
