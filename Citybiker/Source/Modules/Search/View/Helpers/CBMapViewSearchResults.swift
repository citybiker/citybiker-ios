//
//  CBMapViewSearchResults.swift
//  Citybiker
//
//  Created by Vlad on 8/6/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = searchResults?.count ?? 0
        return max(rows, 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell") as! SearchResultTableViewCell
        
        if let results = searchResults, results.count > 0 {
            cell.title = searchResults![(indexPath as NSIndexPath).row]
        } else {
            cell.title = NSLocalizedString("SEARCH_NO_RESULTS", comment: "No results")
        }
        
        return cell
    }
    
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        closeButtonTapped()
        output.didSelectSearchResult(index: indexPath.row)
    }
    
}
