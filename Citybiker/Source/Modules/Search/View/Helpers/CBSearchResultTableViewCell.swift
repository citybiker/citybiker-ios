//
//  SearchResultTableViewCell.swift
//  Citybiker
//
//  Created by Vlad on 8/5/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var resultTitle: UILabel!
    var title: String? {
        didSet {
            resultTitle.text = title
        }
    }
    
}
