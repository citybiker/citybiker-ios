//
//  SearchCBSearchViewController.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import pop

class SearchViewController: UIViewController, SearchViewInput {
    
    var output: SearchViewOutput!
    
    internal var searchResults: [String]?
    
    @IBOutlet weak private var searchButton: UIButton!
    @IBOutlet weak private var hideKeyboardButton: UIButton!
    
    @IBOutlet weak private var searchView: UIView!
    @IBOutlet weak private var headerTitle: UILabel!
    @IBOutlet weak private var searchTextField: UITextField!
    @IBOutlet weak private var searchViewTop: NSLayoutConstraint!
    static private let hidenSearchViewTopConstraint = -100
    static private let shownSearchViewTopConstraint = 27.0
    
    @IBOutlet weak private var searchResultsView: UIView!
    @IBOutlet weak internal var searchResultsTable: UITableView!
    @IBOutlet weak private var searchResultsTableBottom: NSLayoutConstraint!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unregisteForKeyboardNotifications()
    }
    
    
    
    // MARK: - SearchViewInput
    
    func setupInitialState() {
        localizeScreen()
    }
    
    func showSearchButton() {
        let alphaAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
        alphaAnimation?.fromValue = searchButton.alpha
        alphaAnimation?.toValue = 1
        searchButton.pop_add(alphaAnimation, forKey: "showSearchButton")
    }
    
    func hideSearchButton() {
        let alphaAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
        alphaAnimation?.fromValue = searchButton.alpha
        alphaAnimation?.toValue = 0
        searchButton.pop_add(alphaAnimation, forKey: "hideSearchButton")
    }
    
    func show(searchResults results: [String]) {
        searchResults = results
        searchResultsTable.reloadData()
    }
    
    func showError(errorMessage: String) {
        NotificationLabel.showNotification(text: errorMessage)
    }
    
    
    
    // MARK: - Actions
    
    @IBAction internal func searchButtonTapped() {
        Analytics.logSearchButtonTappedEvent()
        
        hideSearchButton()
        showSearchView()
        hideKeyboardButton.isHidden = false
        searchTextField.becomeFirstResponder()
    }
    
    @IBAction internal func hideKeyboardButtonTapped() {
        closeButtonTapped()
    }
    
    @IBAction internal func closeButtonTapped() {
        Analytics.logDismissSearchButtonTappedEvent()
        
        searchTextField.resignFirstResponder()
        searchTextField.text = nil
        
        searchResults = nil
        searchResultsTable.reloadData()
        
        hideSearchView()
        showSearchButton()
        hideSearchResultsView()
        hideKeyboardButton.isHidden = true
    }
    
    @IBAction internal func textFieldValueChanged() {
        let address = searchTextField.text ?? ""
        
        if address.characters.count == 0 {
            searchResults = nil
            searchResultsTable.reloadData()
            
            hideSearchResultsView()
        } else {
            showSearchResultsView()
        }
        
        output.didChangeSearchText(text: address)
    }
    
    
    
    // MARK: - Private Methods
    
    private func localizeScreen() {
        func localizeHeaderTitle() {
            let headerTitleText = NSLocalizedString("SEARCH_HEADER_TITLE", comment: "Header title")
            headerTitle.text = headerTitleText
        }
        
        func localizeSearchTextFieldPlaceholder() {
            let searchTextFieldPlaceholderText = NSLocalizedString("SEARCH_TEXTFIELD_PLACEHOLDER",
                                                                   comment: "Search placeholder")
            let placeholderAttributes = [NSForegroundColorAttributeName : UIColor.cbLightGrayColor()]
            let placeholderAttributedString = NSAttributedString(string: searchTextFieldPlaceholderText,
                                                                 attributes: placeholderAttributes)
            searchTextField.attributedPlaceholder = placeholderAttributedString
        }
        
        localizeHeaderTitle()
        localizeSearchTextFieldPlaceholder()
    }
    
    private func showSearchView() {
        let springAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant)
        springAnimation?.toValue = type(of: self).shownSearchViewTopConstraint
        springAnimation?.springBounciness = 10
        springAnimation?.springSpeed = 8
        searchViewTop.pop_add(springAnimation, forKey: "showSearchView")
    }
    
    private func hideSearchView() {
        let springAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant)
        springAnimation?.toValue = type(of: self).hidenSearchViewTopConstraint
        springAnimation?.springBounciness = 10
        springAnimation?.springSpeed = 8
        searchViewTop.pop_add(springAnimation, forKey: "hideSearchView")
    }
    
    private func showSearchResultsView() {
        guard searchResultsView.alpha < 1 else {
            return
        }
        
        let alphaAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
        alphaAnimation?.fromValue = searchResultsView.alpha
        alphaAnimation?.toValue = 1
        alphaAnimation?.duration = 0.2
        searchResultsView.pop_add(alphaAnimation, forKey: "showSearchResults")
    }
    
    private func hideSearchResultsView() {
        guard searchResultsView.alpha > 0 else {
            return
        }
        
        let alphaAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
        alphaAnimation?.fromValue = searchResultsView.alpha
        alphaAnimation?.toValue = 0
        alphaAnimation?.duration = 0.2
        searchResultsView.pop_add(alphaAnimation, forKey: "hideSearchResults")
    }
    
    
    
    // MARK: Keyboard Notifications
    
    internal func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    internal func unregisteForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardWillShow,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardWillHide,
                                                  object: nil)
    }
    
    internal func keyboardWillShow(_ notification: Notification) {
        let keyboardHeight: CGFloat
        if let keyboardFrameValue = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue {
            keyboardHeight = keyboardFrameValue.cgRectValue.height
        } else {
            keyboardHeight = 0
        }
        
        searchResultsTableBottom.constant = keyboardHeight
        view.layoutIfNeeded()
    }
    
    internal func keyboardWillHide(_ notification: Notification) {
        searchResultsTableBottom.constant = 0
        view.layoutIfNeeded()
    }
    
}
