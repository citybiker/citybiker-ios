//
//  SearchCBSearchViewInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

protocol SearchViewInput: class {
    
    func setupInitialState()
    
    func showSearchButton()
    func hideSearchButton()
    
    func show(searchResults results: [String])
    func showError(errorMessage: String)
    
}
