//
//  CBSearchCBSearchInteractorOutput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit

protocol SearchInteractorOutput: class {
    
    func didSucceedPlacesFetch(searchResults: [MKMapItem])
    func didFailPlacesFetch(error: NSError?)
    
}
