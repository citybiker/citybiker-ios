//
//  CBSearchCBSearchInteractor.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit

class SearchInteractor: SearchInteractorInput {
    
    weak var output: SearchInteractorOutput!
    
    private var addressSearchWorker: MKLocalSearch?
    
    
    // MARK: - SearchInteractorInput
    
    func fetchPlaces(address: String) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            self?.cancelPlacesFetch()
            
            self?.addressSearchWorker = MKLocalSearch(request: (self?.makeLocalSearchRequest(address: address))!)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self?.addressSearchWorker?.start { response, error in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                guard let response = response else {
                    self?.output.didFailPlacesFetch(error: error as NSError?)
                    return
                }
                
                let filteredResponse = response.mapItems.filter({ item -> Bool in
                    return (self?.isItemInsideRegion(item: item))!
                })
                
                DispatchQueue.main.async {
                    self?.output.didSucceedPlacesFetch(searchResults: filteredResponse)
                }
            }
        }
    }
    
    func cancelPlacesFetch() {
        if let addressSearchWorker = self.addressSearchWorker {
            addressSearchWorker.cancel()
        }
    }
    
    
    
    // MARK: - Private Methods
    
    func makeLocalSearchRequest(address: String) -> MKLocalSearchRequest {
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = address
        request.region = MapServices.unitedKingdomRegion()
        
        return request
    }
    
    func isItemInsideRegion(item: MKMapItem) -> Bool {
        let region = MapServices.unitedKingdomExtremePoints()
        let result = item.placemark.coordinate.latitude >= region.southernmostPoint.latitude &&
            item.placemark.coordinate.latitude <= region.northernmostPoint.latitude &&
            item.placemark.coordinate.longitude >= region.westernmostPoint.longitude &&
            item.placemark.coordinate.longitude <= region.easternmostPoint.longitude
        
        return result
    }
    
}
