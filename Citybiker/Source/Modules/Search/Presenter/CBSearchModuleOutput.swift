//
//  CBSearchModuleOutput.swift
//  Citybiker
//
//  Created by Vlad on 10/12/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit

protocol SearchModuleOutput: class {
    
    func didSelectSearchResult(result: MKMapItem)
    
}
