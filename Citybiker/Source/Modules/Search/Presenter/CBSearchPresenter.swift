//
//  SearchCBSearchPresenter.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit

class SearchPresenter: SearchModuleInput, SearchViewOutput, SearchInteractorOutput {
    
    weak var view: SearchViewInput!
    var interactor: SearchInteractorInput!
    var router: SearchRouterInput!
    
    private var output: SearchModuleOutput?
    private var searchResults: [MKMapItem]?
    
    
    // MARK: - SearchModuleInput
    
    func configureModule(delegate: SearchModuleOutput) {
        output = delegate
    }
    
    func showSearchButton() {
        view.showSearchButton()
    }
    
    func hideSearchButton() {
        view.hideSearchButton()
    }
    
    
    
    // MARK: - SearchViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
    }
    
    func didChangeSearchText(text: String) {
        if text.characters.count > 3 {
            interactor.fetchPlaces(address: text)
        }
    }
    
    func didSelectSearchResult(index: Int) {
        output?.didSelectSearchResult(result: searchResults![index])
    }
    
    
    
    // MARK: - SearchInteractorOutput
    
    func didSucceedPlacesFetch(searchResults: [MKMapItem]) {
        self.searchResults = searchResults
        
        let addresses = searchResults.flatMap(addressString)
        view.show(searchResults: addresses)
        Analytics.logSearchResultsShownEvent()
    }
    
    func didFailPlacesFetch(error: NSError?) {
        searchResults = nil
        view.show(searchResults: [String]())
        
        if let errorMessage = error?.userInfo[CitybikerError.descriptionKey] as? String {
            view.showError(errorMessage: errorMessage)
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func addressString(from item: MKMapItem) -> String? {
        if let itemName = item.name {
            if let placemarkTitle = item.placemark.title {
                return ("\(itemName), \(placemarkTitle)")
            } else {
                return itemName
            }
        }
        
        return nil
    }
    
}
