//
//  SearchCBSearchModuleInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 11/10/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

protocol SearchModuleInput: class {
    
    func configureModule(delegate: SearchModuleOutput)
    func showSearchButton()
    func hideSearchButton()
    
}
