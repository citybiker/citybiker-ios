//
//  IntroCBIntroViewController.swift
//  Citybiker
//
//  Created by Vlad Ionita on 08/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import Foundation
import Firebase

class IntroViewController: UIViewController, IntroViewInput {
    
    var output: IntroViewOutput!
    
    var slider: IntroContentViewController? {
        return childViewControllers.flatMap({ $0 as? IntroContentViewController }).first
    }
    
    @IBOutlet weak var bikeTrace: UIImageView!
    @IBOutlet weak var bikeTraceLeading: NSLayoutConstraint!
    
    @IBOutlet weak var bike: BikeView!
    
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var goButtonTrailing: NSLayoutConstraint!
    @IBOutlet weak var goButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var goButtonWidth: NSLayoutConstraint!
    
    @IBOutlet weak var pageIndicatorImage: UIImageView!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.logTutorialBeginEvent()
        slider!.didScrollToXOffset = didScroll
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    // TODO: localize the screen (at least the Go button)
    
    
    // MARK: - Actions
    
    @IBAction func goButtonTapped() {
        output.didTapGoButton()
        Analytics.logTutorialCompleteEvent()
        // TODO: rotate the go button to the map as a wheel
    }
    
    
    
    // MARK: - Private Methods
    
    func didScroll(toOffset xOffset: CGFloat) {
        let scrollWidth = slider!.scrollView.frame.width
        let numberOfPagesScrolled = xOffset / scrollWidth
        
        let bikeTraceFirst: CGFloat = 31
        let bikeTraceSecond: CGFloat = 106
        let bikeTraceThird: CGFloat = 207
        
        let bikeTraceVisibleWidth: CGFloat
        if numberOfPagesScrolled <= 1 {
            bikeTraceVisibleWidth = bikeTraceFirst + numberOfPagesScrolled * (bikeTraceSecond - bikeTraceFirst)
        } else {
            bikeTraceVisibleWidth = bikeTraceSecond + (numberOfPagesScrolled - 1) * (bikeTraceThird - bikeTraceSecond)
        }
        bikeTraceLeading.constant = bikeTraceVisibleWidth - bikeTrace.frame.width
        
        let size = (xOffset - 380) / 300 * 94
        goButtonTrailing.constant = xOffset - 668
        goButtonWidth.constant = size
        goButtonHeight.constant = size
        goButton.layer.cornerRadius = size / 2
        
        // TODO: rotate the go button as the finish animation
        
        
        if numberOfPagesScrolled < 0.5 {
            pageIndicatorImage.image = UIImage(named: "intro_page_indicator_1")
        } else if numberOfPagesScrolled < 1.5 {
            pageIndicatorImage.image = UIImage(named: "intro_page_indicator_2")
        } else {
            pageIndicatorImage.image = UIImage(named: "intro_page_indicator_3")
        }
        
        bike.set(scrollingPosition: xOffset, inPage: numberOfPagesScrolled)
    }
    
}
