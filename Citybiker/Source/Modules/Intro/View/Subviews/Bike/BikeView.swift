//
//  BikeView.swift
//  Citybiker
//
//  Created by Vlad on 8/8/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class BikeView: UIView {
    
    static private let kRearWheelDiameter: CGFloat = 29
    static private let kFrontWheelDiameter: CGFloat = 46
    
    static let kInitialPosition: CGFloat = 30
    static let kMiddlePosition: CGFloat = 120
    static let kFinalPosition: CGFloat = 220
    
    @IBOutlet weak var viewLeading: NSLayoutConstraint!
    @IBOutlet weak var rearWheel: BikeWheelImageView!
    @IBOutlet weak var frontWheel: BikeWheelImageView!
    
    // MARK: - View LifeCycle
    
    override func awakeFromNib() {
        rearWheel.diameter = type(of: self).kRearWheelDiameter
        frontWheel.diameter = type(of: self).kFrontWheelDiameter
    }
    
    
    
    // MARK: - Public Methods
    
    func set(scrollingPosition position: CGFloat, inPage page: CGFloat) {
        let initialPositionInPage = page <= 1 ? type(of: self).kInitialPosition : type(of: self).kMiddlePosition
        let distanceScrolledInPage = page <= 1 ? page : page - 1
        let distanceToMoveInPage = page <= 1 ? BikeView.kMiddlePosition - BikeView.kInitialPosition : BikeView.kFinalPosition - BikeView.kMiddlePosition
        
        set(position: initialPositionInPage + distanceScrolledInPage * distanceToMoveInPage)
    }
    
    
    
    // MARK: - Private Methods
    
    private func set(position: CGFloat) {
        viewLeading.constant = position
        
        rearWheel.rotate(dinstanceDelta: position - type(of: self).kInitialPosition)
        frontWheel.rotate(dinstanceDelta: position - type(of: self).kInitialPosition)
    }
    
}
