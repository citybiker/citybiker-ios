//
//  BikeWheelImageView.swift
//  Citybiker
//
//  Created by Vlad on 8/9/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class BikeWheelImageView: UIImageView {
    
    var diameter: CGFloat!
    
    // MARK: - Public Methods
    
    func rotate(dinstanceDelta distance: CGFloat) {
        let circumference = circumferenceLength(from: diameter)
        let rotationInPercents = distance / circumference
        
        rotate(rotationInPercents: rotationInPercents)
    }
    
    
    
    // MARK: - Private Methods
    
    private func rotate(rotationInPercents rotation: CGFloat) {
        let rotationsInDegrees = rotation * 360
        let rotationsInRadians = radians(from: rotationsInDegrees)
        
        self.transform = CGAffineTransform(rotationAngle: rotationsInRadians)
    }
    
    
    // MARK: Math Helpers
    
    private func circumferenceLength(from circleDiameter: CGFloat) -> CGFloat {
        return CGFloat(M_PI) * circleDiameter
    }
    
    private func radians(from degrees: CGFloat) -> CGFloat {
        return CGFloat(Double(degrees) * M_PI / 180)
    }

}
