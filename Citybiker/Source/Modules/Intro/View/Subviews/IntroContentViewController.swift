//
//  IntroContentViewController.swift
//  Citybiker
//
//  Created by Vlad on 8/8/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class IntroContentViewController: UIViewController, UIScrollViewDelegate {
    
    private let contentImages = [UIImage(named: "intro_step_1"),
                                 UIImage(named: "intro_step_2"),
                                 UIImage(named: "intro_step_3")]
    var didScrollToXOffset: ((CGFloat) -> ())?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        prepareScrollView()
    }
    
    
    
    // MARK: - Private Methods
    
    func prepareScrollView() {
        let viewWidth = self.view.frame.size.width
        let scrollWidth = self.scrollView.frame.size.width
        let scrollHeight = self.scrollView.frame.size.height
        let scrollGutterWidth = scrollWidth - viewWidth
        
        for (index, image) in contentImages.enumerated() {
            let xOffset = CGFloat(index) * (viewWidth + scrollGutterWidth) + scrollGutterWidth / 2
            
            let imageView = UIImageView(image: image)
            imageView.frame = CGRect(x: xOffset, y: 0, width: viewWidth, height: scrollHeight)
            
            scrollView.addSubview(imageView)
        }
        
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        scrollView.contentSize = CGSize(width: scrollWidth * CGFloat(contentImages.count),
                                            height: scrollHeight);
    }
    
    
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetX = scrollView.contentOffset.x
        didScrollToXOffset?(offsetX)        
    }
    
}
