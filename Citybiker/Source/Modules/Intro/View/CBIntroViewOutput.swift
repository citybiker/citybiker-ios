//
//  IntroCBIntroViewOutput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 08/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

protocol IntroViewOutput {
    
    func didTapGoButton()
    
}
