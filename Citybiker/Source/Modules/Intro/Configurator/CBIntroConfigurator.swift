//
//  CBIntroCBIntroConfigurator.swift
//  Citybiker
//
//  Created by Vlad Ionita on 08/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class IntroModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? IntroViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: IntroViewController) {
        let router = IntroRouter()
        
        let presenter = IntroPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = IntroInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
