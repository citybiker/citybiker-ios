//
//  CBIntroCBIntroInitializer.swift
//  Citybiker
//
//  Created by Vlad Ionita on 08/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class IntroModuleInitializer: NSObject {
    
    @IBOutlet weak var introViewController: IntroViewController!
    
    override func awakeFromNib() {
        let configurator = IntroModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: introViewController)
    }
    
}
