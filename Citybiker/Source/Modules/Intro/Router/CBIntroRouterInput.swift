//
//  IntroCBIntroRouterInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 08/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

protocol IntroRouterInput {
    
    func showMapScreen(sourceController: UIViewController)
    
}
