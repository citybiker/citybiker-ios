//
//  IntroCBIntroRouter.swift
//  Citybiker
//
//  Created by Vlad Ionita on 08/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class IntroRouter: IntroRouterInput {
    
    func showMapScreen(sourceController: UIViewController) {
        let map = UIStoryboard(name: "Map", bundle: nil).instantiateInitialViewController()
        
        if let window = UIApplication.shared.delegate?.window {
            UIView.transition(with: window!,
                              duration: 0.3,
                              options: .transitionFlipFromRight,
                              animations: {
                                window?.rootViewController = map
            })
        }
    }
    
}
