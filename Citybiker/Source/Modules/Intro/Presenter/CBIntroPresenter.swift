//
//  IntroCBIntroPresenter.swift
//  Citybiker
//
//  Created by Vlad Ionita on 08/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class IntroPresenter: IntroModuleInput, IntroViewOutput, IntroInteractorOutput {
	
    weak var view: IntroViewInput!
    var interactor: IntroInteractorInput!
    var router: IntroRouterInput!
    
    
    // MARK: - IntroViewOutput
    
    func didTapGoButton() {
        let viewController = view as! UIViewController
        router.showMapScreen(sourceController: viewController)
        
        setAppLaunched()
    }
    
    
    
    // MARK: - Private Methods
    
    private func setAppLaunched() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: kAppLaunched)
        userDefaults.synchronize()
    }
    
}
