//
//  CBMapCBMapInteractorOutput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import MapKit

protocol MapInteractorOutput: class {
    
    func didAuthorizeLocationPermissions()
    func didNotAuthorizeLocationPermissions()
    func didUpdateLocation(newLocation: CLLocationCoordinate2D)
    
    func didSucceedAddressFetch(address: String)
    
    func didSucceedRetrieveParkingSlots(parkingSlots: Set<ParkingSlot>)
    func didFailRetrieveParkingSlots(error: NSError?)
    
    func didSucceedRetrieveRoute(route: Route)
    func didFailRetrieveRoute(error: NSError?)
    
}
