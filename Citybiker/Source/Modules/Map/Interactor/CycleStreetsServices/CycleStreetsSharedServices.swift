//
//  CycleStreetsSharedService.swift
//  Citybiker
//
//  Created by Vlad on 8/10/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

class CycleStreetsSharedServices {
    
    static let apiV2Path = "https://api.cyclestreets.net/v2/"
    static let apiV1Path = "https://www.cyclestreets.net/api/"
    
    static let cycleStreetsKey = "9ad0c0aad182bf0e"
    
}
