//
//  RetrieveRouteRequest.swift
//  Citybiker
//
//  Created by Vlad on 8/10/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import GoogleMaps

class RetrieveRouteRequest {
    
    static private let routePlan =  "balanced"
    
    let startingPoint: CLLocationCoordinate2D
    let destinationPoint: CLLocationCoordinate2D
    
    init(startingPoint: CLLocationCoordinate2D, destinationPoint: CLLocationCoordinate2D) {
        self.startingPoint = startingPoint
        self.destinationPoint = destinationPoint
    }
    
    func parameters() -> JSONDictionary {
        return [
            "plan": type(of: self).routePlan,
            "itinerarypoints": "\(startingPoint.longitude),\(startingPoint.latitude)|\(destinationPoint.longitude),\(destinationPoint.latitude)",
            "key": CycleStreetsSharedServices.cycleStreetsKey,
        ]
    }
    
}
