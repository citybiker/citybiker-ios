//
//  RetrieveRouteService.swift
//  Citybiker
//
//  Created by Vlad on 8/10/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import AlamofireDomain

class RetrieveRouteService {
    
    private let routesApiPath = CycleStreetsSharedServices.apiV1Path + "journey.json"
    
    func retrieveRoute(request: RetrieveRouteRequest, completion: @escaping (Bool, RetrieveRouteResponse?, NSError?) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        AlamofireDomain.request(routesApiPath, method: .get, parameters: request.parameters(), encoding: URLEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch response.result {
                case .success(let value):
                    guard let json = value as? JSONDictionary else {
                        let error = RetrieveRouteErrors().noDataError()
                        completion(false, nil, error)
                        
                        return
                    }
                    
                    let response = RetrieveRouteResponse(json: json, request: request)
                    completion(true, response, nil)
                case .failure(_):
                    guard let statusCode = response.response?.statusCode else {
                        let error = RetrieveRouteErrors().noStatusCodeError()
                        completion(false, nil, error)
                        
                        return
                    }
                    
                    let error = NSError(domain: RetrieveParkingSlotsErrors.domain,
                        code: statusCode,
                        userInfo: nil)
                    completion(false, nil, error)
                }
        }
    }
    
}
