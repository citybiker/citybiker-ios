//
//  RetrieveRouteResponse.swift
//  Citybiker
//
//  Created by Vlad on 8/10/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class RetrieveRouteResponse {
    
    var route: Route?
    
    init(json: JSONDictionary, request: RetrieveRouteRequest) {
        if let waypoints = json["waypoint"] as? Array<JSONDictionary>,
        let routeSegments = json["marker"] as? Array<JSONDictionary> {
            route = Route(routeSegments: routeSegments, waypoints: waypoints)
            route!.originalStartPoint = request.startingPoint
            route!.originalEndPoint = request.destinationPoint
        }
    }
    
}

class Route {
    
    var originalStartPoint: CLLocationCoordinate2D?
    var originalEndPoint: CLLocationCoordinate2D?
    
    var startPoint: CLLocationCoordinate2D?
    var endPoint: CLLocationCoordinate2D?
    
    var segments: Array<RouteSegment>?
    var bounds: PlaceBounds?
    
    var length: Double?
    var time: Double?
    
    
    init(routeSegments: Array<JSONDictionary>, waypoints: Array<JSONDictionary>) {
        if let firstWaypoint = waypoints.first {
            startPoint = coordinateFromWaypoint(firstWaypoint)
        }
        
        if let lastWaypoint = waypoints.last {
            endPoint = coordinateFromWaypoint(lastWaypoint)
        }
        
        if let firstSegment = routeSegments.first {
            bounds = routeBounds(from: firstSegment)
            time = routeTime(from: firstSegment)
            length = routeLength(from: firstSegment)
        }
        
        segments = routeSegments.flatMap(parse)
    }
    
    
    
    // MARK: - Private Methods
    
    private func routeBounds(from segment: JSONDictionary) -> PlaceBounds? {
        guard let attributes = segment["@attributes"] as? JSONDictionary else {
            return nil
        }
        
        guard let northPointString = attributes["north"] as? String,
            let northPoint = Double(northPointString),
            let southPointString = attributes["south"] as? String,
            let southPoint = Double(southPointString),
            let westPointString = attributes["west"] as? String,
            let westPoint = Double(westPointString),
            let eastPointString = attributes["east"] as? String,
            let eastPoint = Double(eastPointString) else {
            return nil
        }
        
        return PlaceBounds(north: northPoint, east: eastPoint, south: southPoint, west: westPoint)
    }
    
    private func routeTime(from segment: JSONDictionary) -> Double? {
        guard let attributes = segment["@attributes"] as? JSONDictionary else {
            return nil
        }
        
        guard let timeString = attributes["time"] as? String,
            let time = Double(timeString) else {
                return nil
        }
        
        return time
    }
    
    private func routeLength(from segment: JSONDictionary) -> Double? {
        guard let attributes = segment["@attributes"] as? JSONDictionary else {
            return nil
        }
        
        guard let lengthString = attributes["length"] as? String,
            let length = Double(lengthString) else {
                return nil
        }
        
        return length
    }
    
    private func parse(json: JSONDictionary) -> RouteSegment? {
        guard let segmentAttributes = json["@attributes"] as? JSONDictionary else {
            return nil
        }
        
        return RouteSegment(json: segmentAttributes)
    }
    
    private func coordinateFromWaypoint(_ waypoint: JSONDictionary) -> CLLocationCoordinate2D? {
        guard let attributes = waypoint["@attributes"] as? JSONDictionary,
            let longitudeString = attributes["longitude"] as? String,
            let latitudeString = attributes["latitude"] as? String else {
                return nil
        }
        
        guard let longitude = Double(longitudeString),
            let latitude = Double(latitudeString) else {
                return nil
        }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
}

class RouteSegment {
    
    var coordinates: Array<CLLocationCoordinate2D>?
    
    init?(json: JSONDictionary) {
        guard let coordinatesString = json["points"] as? String else {
            return nil
        }

        coordinates = coordinatesString.components(separatedBy: " ").flatMap(coordinateFromString)
    }
    
    
    private func coordinateFromString(_ coordinateString: String) -> CLLocationCoordinate2D? {
        let coordinateComponents = coordinateString.components(separatedBy: ",")
        
        guard let longitudeString = coordinateComponents.first, let latitudeString = coordinateComponents.last else {
            return nil
        }
        
        guard let longitude = Double(longitudeString), let latitude = Double(latitudeString) else {
            return nil
        }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
}
