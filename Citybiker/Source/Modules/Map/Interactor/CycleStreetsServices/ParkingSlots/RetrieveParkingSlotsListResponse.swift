//
//  RetrieveParkingSlotsListResponse.swift
//  Citybiker
//
//  Created by Vlad on 8/6/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import CoreLocation

class RetrieveParkingSlotsListResponse {
    
    var parkingSlots: Set<ParkingSlot>?
    
    init(json: JSONDictionary) {
        if let rawParkingSlots = json["features"] as? [JSONDictionary] {
            parkingSlots = Set(rawParkingSlots.flatMap(parse))
        }
    }
    
    private func parse(json: JSONDictionary) -> ParkingSlot? {
        return ParkingSlot(json: json)
    }
    
}

class ParkingSlot: Hashable {
    
    let id: Int
    let description: String?
    let coordinates: CLLocationCoordinate2D
    
    init?(json: JSONDictionary) {
        guard let properties = json["properties"] as? JSONDictionary,
            let id = properties["id"] as? NSNumber else {
                return nil
        }
        
        guard let geometry = json["geometry"] as? JSONDictionary,
            let coordinatesArray = geometry["coordinates"] as? [NSNumber],
            let longitude = coordinatesArray.first?.doubleValue,
            let latitude = coordinatesArray.last?.doubleValue else {
            return nil
        }
        
        self.id = id.intValue
        description = properties["caption"] as? String
        coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(id: Int, description: String?, coordinates: CLLocationCoordinate2D) {
        self.id = id
        self.description = description
        self.coordinates = coordinates
    }
    
    
    var hashValue: Int {
        return "\(id),\(description),\(coordinates.latitude),\(coordinates.longitude)".hashValue
    }
    
}

func ==(lhs:ParkingSlot, rhs: ParkingSlot) -> Bool {
    return lhs.id == rhs.id &&
        lhs.description == rhs.description &&
        lhs.coordinates.latitude == rhs.coordinates.latitude &&
        lhs.coordinates.longitude == rhs.coordinates.longitude
}
