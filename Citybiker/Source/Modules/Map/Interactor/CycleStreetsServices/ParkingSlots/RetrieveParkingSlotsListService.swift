//
//  RetrieveParkingSlotsListServices.swift
//  Citybiker
//
//  Created by Vlad on 8/6/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import AlamofireDomain

open class RetrieveParkingSlotsListService {
    
    private let parkingLotsApiPath = CycleStreetsSharedServices.apiV2Path + "photomap.locations"
    
    private var currentRequest: DataRequest?
    
    
    func retrieveParkingSlotsList(request: RetrieveParkingSlotsListRequest, completion: @escaping (Bool, RetrieveParkingSlotsListResponse?, NSError?) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        currentRequest = AlamofireDomain.request(parkingLotsApiPath, method: .get, parameters: request.parameters(), encoding: URLEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch response.result {
                case .success(let value):
                    guard let json = value as? JSONDictionary else {
                        let error = RetrieveParkingSlotsErrors().noDataError()
                        completion(false, nil, error)
                        
                        return
                    }
                    
                    let response = RetrieveParkingSlotsListResponse(json: json)
                    completion(true, response, nil)
                case .failure(let error):
                    print(error)
                    guard let statusCode = response.response?.statusCode else {
                        let resultError = RetrieveParkingSlotsErrors().noStatusCodeError()
                        completion(false, nil, resultError)
                        
                        return
                    }
                    
                    let resultError = NSError(domain: RetrieveParkingSlotsErrors.domain,
                                              code: statusCode,
                                              userInfo: nil)
                    completion(false, nil, resultError)
                }
        }
    }
    
    func cancelRequest() {
        currentRequest?.cancel()
    }
    
}
