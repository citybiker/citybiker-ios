//
//  RetrieveParkingSlotsListRequest.swift
//  Citybiker
//
//  Created by Vlad on 8/6/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import GoogleMaps

class RetrieveParkingSlotsListRequest {
    
    static private let methodPath = "photomap.locations"
    static private let category = "cycleparking"
    static private let metacategory = "other"
    static private let fields = "id,caption"
    
    let limit: Int
    let southWestCoordinate: CLLocationCoordinate2D
    let northEastCoordinate: CLLocationCoordinate2D
    
    init(limit: Int, southWestCoordinate: CLLocationCoordinate2D, northEastCoordinate: CLLocationCoordinate2D) {
        self.limit = limit
        self.southWestCoordinate = southWestCoordinate
        self.northEastCoordinate = northEastCoordinate
    }
    
    func parameters() -> JSONDictionary {
        return [
            "category": RetrieveParkingSlotsListRequest.category,
            "metacategory": RetrieveParkingSlotsListRequest.metacategory,
            "fields": RetrieveParkingSlotsListRequest.fields,
            "limit": limit,
            "bbox": "\(southWestCoordinate.longitude),\(southWestCoordinate.latitude),\(northEastCoordinate.longitude),\(northEastCoordinate.latitude)",
            "key": CycleStreetsSharedServices.cycleStreetsKey,
        ]
    }
    
}
