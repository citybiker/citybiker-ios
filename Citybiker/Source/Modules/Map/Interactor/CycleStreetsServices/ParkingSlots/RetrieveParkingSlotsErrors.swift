//
//  RetrieveParkingSlotsErrors.swift
//  Citybiker
//
//  Created by Vlad on 8/13/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

struct RetrieveParkingSlotsErrors {
    
    static let domain = "RetrieveParkingSlotsListService"
    
    struct Codes {
        static let noData = 1
        static let noStatusCode = 2
        static let noParkingSlots = 3
    }
    
    func noDataError() -> NSError {
        let errorDescription = NSLocalizedString("GENERAL_ERROR",
                                                 comment: "General error")
        return NSError(domain: type(of: self).domain,
                       code: type(of: self).Codes.noData,
                       userInfo: [CitybikerError.descriptionKey : errorDescription])
    }
    
    func noStatusCodeError() -> NSError {
        let errorDescription = NSLocalizedString("GENERAL_ERROR",
                                                 comment: "General error")
        return NSError(domain: type(of: self).domain,
                       code: type(of: self).Codes.noStatusCode,
                       userInfo: [CitybikerError.descriptionKey : errorDescription])
    }
    
    func noParkingSlotsError() -> NSError {
        let errorDescription = NSLocalizedString("MAP_NO_PARKING_ERROR",
                                                 comment: "No parking slots error")
        return NSError(domain: type(of: self).domain,
                       code: type(of: self).Codes.noParkingSlots,
                       userInfo: [CitybikerError.descriptionKey : errorDescription])
    }
    
}
