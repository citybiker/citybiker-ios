//
//  CBMapCBMapInteractor.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit
import GoogleMaps

class MapInteractor: NSObject, MapInteractorInput, CLLocationManagerDelegate {
    
    weak var output: MapInteractorOutput!
    
    private let locationManager = CLLocationManager()
    private let parkingSlotsService = RetrieveParkingSlotsListService()
    
    
    // MARK: - MapInteractorInput
    
    func requestLocationPermissions() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
        
    func fetchAddress(forLocation locationCoordinates: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: locationCoordinates.latitude,
                                  longitude: locationCoordinates.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
            var addressString: String = ""
            if let placemark = placemarks?.first {
                if let placemarkPostalCode = placemark.postalCode {
                    addressString.append(placemarkPostalCode)
                }
                
                if let placemarkName = placemark.name {
                    addressString.append(", ")
                    addressString.append(placemarkName)
                }
                
                if let placemarkCity = placemark.locality {
                    addressString.append(", ")
                    addressString.append(placemarkCity)
                }
            }
            
            if addressString.characters.count == 0 {
                addressString = "\(locationCoordinates.latitude):\(locationCoordinates.longitude)"
            }
            
            self?.output.didSucceedAddressFetch(address: addressString)
        }
    }
    
    func cancelAddressFetch() {
        CLGeocoder().cancelGeocode()
    }
    
    func retrieveParkingSlots(region: GMSVisibleRegion) {
        parkingSlotsService.cancelRequest()
        
        let request = RetrieveParkingSlotsListRequest(limit: 150,
                                                      southWestCoordinate: region.nearLeft,
                                                      northEastCoordinate: region.farRight)
        parkingSlotsService.retrieveParkingSlotsList(request: request) {
            [weak self] (success, response, error) in
            if success {
                if let parkingSlots = response?.parkingSlots {
                    self?.output.didSucceedRetrieveParkingSlots(parkingSlots: parkingSlots)
                }
            } else {
                self?.output.didFailRetrieveParkingSlots(error: error!)
            }
        }
    }
    
    func retrieveRoute(start: CLLocationCoordinate2D, finish: CLLocationCoordinate2D) {
        let request = RetrieveRouteRequest(startingPoint: start, destinationPoint: finish)
        RetrieveRouteService().retrieveRoute(request: request) {
            [weak self] (success, response, error) in
            if success {
                if let route = response?.route {
                    self?.output.didSucceedRetrieveRoute(route: route)
                } else {
                    let error = RetrieveRouteErrors().noRouteError()
                    self?.output.didFailRetrieveRoute(error: error)
                }
            } else {
                self?.output.didFailRetrieveParkingSlots(error: error!)
            }
        }
    }
    
    
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            self.output.didAuthorizeLocationPermissions()
        case .denied, .restricted:
            self.output.didNotAuthorizeLocationPermissions()
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.output.didUpdateLocation(newLocation: location.coordinate)
        }
    }
    
}
