//
//  CBMapCBMapInteractorInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import GoogleMaps

protocol MapInteractorInput {
    
    func requestLocationPermissions()
    
    func startUpdatingLocation()
    func stopUpdatingLocation()
    
    func fetchAddress(forLocation locationCoordinates: CLLocationCoordinate2D)
    func cancelAddressFetch()
    
    func retrieveParkingSlots(region: GMSVisibleRegion)
    func retrieveRoute(start: CLLocationCoordinate2D, finish: CLLocationCoordinate2D)
    
}
