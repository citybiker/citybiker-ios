//
//  MapCBMapPresenter.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit
import GoogleMaps
import Firebase

class MapPresenter: MapViewOutput, MapInteractorOutput {
	
    weak var view: MapViewInput!
    var interactor: MapInteractorInput!
    var router: MapRouterInput!
    
    internal var routeIsReadySemaphore: DispatchGroup?
    internal var route: Route?
    internal var destinationAddress: String?
    
    internal var searchModule: SearchModuleInput!
    internal var routeDetailsModule: RouteDetailsModuleInput?
    
    
    // MARK: - MapViewOutput

    func viewIsReady() {
        view.setupInitialState()
        interactor.requestLocationPermissions()
        
        showSearchScreen()
    }
    
    func didChangeMapRegion(newRegion: GMSVisibleRegion) {
        interactor.retrieveParkingSlots(region: newRegion)
    }
    
    func didSelectDestinationLocation(destinationLocation: CLLocationCoordinate2D) {
        retrieveRoute(toLocation: destinationLocation, zoomToFit: true)
    }
    
    
    
    // MARK: - MapInteractorOutput
    
    func didAuthorizeLocationPermissions() {
        view.showMyLocationDot()
        interactor.startUpdatingLocation()
    }
    
    func didNotAuthorizeLocationPermissions() {
        self.view.showLocationAccessDeniedError()
    }
    
    func didUpdateLocation(newLocation: CLLocationCoordinate2D) {
        view.moveMapCameraToLocation(newLocation)
        interactor.stopUpdatingLocation()
    }
    
    func didSucceedRetrieveParkingSlots(parkingSlots: Set<ParkingSlot>) {
        let parkingMarkers = Set(parkingSlots.flatMap(parkingMarker))
        view.showParkingMarkers(parkingMarkers)
    }
    
    func didFailRetrieveParkingSlots(error: NSError?) {
        //-- Do nothing, the results are just empty
    }
    
    func didSucceedRetrieveRoute(route: Route) {
        self.route = route
        
        if routeIsReadySemaphore != nil {
            routeIsReadySemaphore!.leave()
        }
    }
    
    func didFailRetrieveRoute(error: NSError?) {
        if let errorMessage = error?.userInfo[CitybikerError.descriptionKey] as? String {
            view.showError(errorMessage: errorMessage)
        }
    }
    
    func didSucceedAddressFetch(address: String) {
        destinationAddress = address
        
        if routeIsReadySemaphore != nil {
            routeIsReadySemaphore!.leave()
        }
    }
    
    
    
    // MARK: - Private Methods
    
    internal func retrieveRoute(toLocation destinationLocation: CLLocationCoordinate2D, zoomToFit: Bool) {
        guard let currentLocation = view.currentLocation else {
            view.showLocationAccessDeniedError()
            return
        }
        
        Analytics.logSelectedDestinationEvent()
        routeIsReadySemaphore = DispatchGroup()
        routeIsReadySemaphore!.enter()
        routeIsReadySemaphore!.enter()
        
        interactor.retrieveRoute(start: currentLocation, finish: destinationLocation)
        interactor.fetchAddress(forLocation: destinationLocation)
        
        routeIsReadySemaphore!.notify(queue: DispatchQueue.main) { [weak self] in
            if let welf = self {
                welf.searchModule.hideSearchButton()
                
                welf.view.showRoute(welf.route!, zoomToFit: zoomToFit)
                if welf.routeDetailsModule == nil {
                    welf.showRouteDetailsScreen()
                }
                welf.routeDetailsModule!.setRouteDetails(address: self!.destinationAddress!,
                                                         time: welf.route!.time!,
                                                         length: welf.route!.length!)
                
                welf.routeIsReadySemaphore = nil
            }
        }
    }
    
    
    
    // MARK: - Convenience Methods
    
    private func parkingMarker(from parkingSlot: ParkingSlot) -> ParkingMarker {
        let spacesAmount = numberOfSpaces(from: parkingSlot.description)
        return ParkingMarker(id: parkingSlot.id, position: parkingSlot.coordinates, numberOfSpaces: spacesAmount)
    }
    
    private func numberOfSpaces(from description: String?) -> Int {
        var numberOfSpaces: Int = 0
        if let description = description {
            let allDigits = description.components(separatedBy: CharacterSet.decimalDigits.inverted)
            
            if let firstDigitString = allDigits.first, let firstDigit = Int(firstDigitString) {
                numberOfSpaces = firstDigit
            }
        }
        
        return numberOfSpaces
    }
    
}
