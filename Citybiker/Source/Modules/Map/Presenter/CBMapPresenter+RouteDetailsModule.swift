//
//  CBMapPresenter+RouteDetailsModuleOutput.swift
//  Citybiker
//
//  Created by Vlad on 10/12/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

extension MapPresenter: RouteDetailsModuleOutput {
    
    func userDidTapRefresh() {
        retrieveRoute(toLocation: route!.originalEndPoint!, zoomToFit: false)
    }
    
    func userDidTapClose() {
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) { [weak self] in
            self?.hideRouteDetailsScreen()
        }
        
        view.removeRoute()
        searchModule.showSearchButton()
    }
    
}
