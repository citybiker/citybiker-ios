//
//  CBMapPresenter+Routing.swift
//  Citybiker
//
//  Created by Vlad on 10/20/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

extension MapPresenter {
    
    internal func showSearchScreen() {
        let viewController = view as! UIViewController
        searchModule = router.showSearchScreen(onParentController: viewController, containerView: view.searchContainer)
        searchModule.configureModule(delegate: self)
    }
    
    internal func showRouteDetailsScreen() {
        let viewController = view as! UIViewController
        routeDetailsModule = router.showRouteDetailsScreen(onParentController: viewController, containerView: view.routeDetailsContainer)
        routeDetailsModule!.configureModule(delegate: self)
    }
    
    internal func hideRouteDetailsScreen() {
        let routeDetailsPresenter = routeDetailsModule as! RouteDetailsPresenter
        let routeDetailsController = routeDetailsPresenter.view as! RouteDetailsViewController
        router.hideScreen(screenController: routeDetailsController)
        routeDetailsModule = nil
    }
    
}
