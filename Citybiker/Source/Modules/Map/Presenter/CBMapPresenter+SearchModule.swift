//
//  CBMapPresenter+SearchModuleOutput.swift
//  Citybiker
//
//  Created by Vlad on 10/12/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit

extension MapPresenter: SearchModuleOutput {
    
    func didSelectSearchResult(result: MKMapItem) {
        view.moveMapCameraAndShowSearchResultMarker(for: result)
    }
    
}
