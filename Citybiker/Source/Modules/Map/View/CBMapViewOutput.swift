//
//  MapCBMapViewOutput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import GoogleMaps

protocol MapViewOutput {
    
    func viewIsReady()
    
    func didChangeMapRegion(newRegion: GMSVisibleRegion)
    func didSelectDestinationLocation(destinationLocation: CLLocationCoordinate2D)
    
}
