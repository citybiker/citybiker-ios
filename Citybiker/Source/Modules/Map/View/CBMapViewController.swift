//
//  MapCBMapViewController.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import Firebase

class MapViewController: UIViewController, MapViewInput {
    
    static internal let kDestinationMarkerSnippet = "Destination Marker"
    static internal let kSearchResultMarkerSnippet = "Search Result Marker"
    
    var output: MapViewOutput!
    
    var mainView: MapView { return self.view as! MapView }
    var currentLocation: CLLocationCoordinate2D? { return mainView.googleMap.myLocation?.coordinate }
    var searchContainer: UIView { return mainView.searchContainerView }
    var routeDetailsContainer: UIView { return mainView.routeDetailsContainerView }
    
    var lastZoomLevel: ZoomLevel = .streetLevel
    
    var parkingMarkersContainer = MarkersSafeContainer<ParkingMarker>()
    var tappedMarkersContainer = MarkersSafeContainer<ParkingMarker>()
    var parkingMarkersOverlaysContainer = MarkerOverlaysSafeContainer()
    var searchResultMarker: SearchResultMarker?
    var destinationMarker: GMSMarker?
    var routePolyline: RoutePolyline?
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - MapViewInput
    
    func setupInitialState() {
        UIApplication.shared.statusBarStyle = .default
        
        configureMap()
    }
    
    func showMyLocationDot() {
        mainView.showMyLocationDot(show: true)
    }
    
    func showLocationAccessDeniedError() {
        self.present(locationAccessDeniedAlert(), animated: true, completion: nil)
    }
    
    func moveMapCameraToLocation(_ location: CLLocationCoordinate2D) {
        mainView.moveCameraToLocation(location)
    }
    
    func moveMapCameraAndShowSearchResultMarker(for mapItem: MKMapItem) {
        let markerPosition = mapItem.placemark.coordinate
        let newSearchResultMarker = SearchResultMarker(position: markerPosition)
        newSearchResultMarker.snippet = type(of: self).kSearchResultMarkerSnippet
        
        if searchResultMarker != nil {
            removeSearchResultMarker()
        }
        mainView.addMarker(newSearchResultMarker)
        searchResultMarker = newSearchResultMarker
        
        mainView.animateCameraMovesAndZoomToSeeLocation(markerPosition)
    }
    
    func showParkingMarkers(_ parkingMarkersInCurrentRegion: Set<ParkingMarker>) {
        let currentParkingMarkers = parkingMarkersContainer.markers()
        guard currentParkingMarkers != parkingMarkersInCurrentRegion else {
            return
        }
        
        let parkingMarkersInCurrentRegion = parkingMarkersInCurrentRegion.subtracting(tappedMarkersContainer.markers())
        let newMarkers = parkingMarkersInCurrentRegion.subtracting(currentParkingMarkers)
        updateParkingMarkersIcon(markers: newMarkers)
        newMarkers.forEach { marker in
            mainView.addMarker(marker.marker)
        }
        
        let possibleOldMarkers = currentParkingMarkers.subtracting(parkingMarkersInCurrentRegion)
        let oldMarkers = possibleOldMarkers.filter{ !isLocationVisible(location: $0.marker.position) }
        oldMarkers.forEach { mainView.removeMarker($0.marker) }
        
        parkingMarkersContainer.removeMarkers(markers: Set<ParkingMarker>(oldMarkers))
        parkingMarkersContainer.addMarkers(markers: newMarkers)
    }
    
    func showRoute(_ route: Route, zoomToFit: Bool) {
        removeSelectionOverlays()
        removeAllParkingMarkers()
        _removeRoute()
        
        if let destinationCoordinates = route.originalEndPoint {
            showDestinationMarker(markerCoordinates: destinationCoordinates)
            
            if searchResultMarker != nil && destinationMarker != nil && searchResultMarker!.position == destinationMarker!.position {
                removeSearchResultMarker()
            }
        }
        
        self.routePolyline = mainView.showRouteLine(route)
        if zoomToFit {
            let edgeInsets = UIEdgeInsetsMake(150, 20, 20, 20)
            mainView.fitRoutePath(self.routePolyline!.path, with: edgeInsets)
        }
    }
    
    func removeRoute() {
        _removeRoute()
        retrieveParkingSpacesForCurrentRegion()
    }
    
    func showError(errorMessage: String) {
        NotificationLabel.showNotification(text: errorMessage)
    }
    
    
    
    // MARK: - Actions
    
    @IBAction internal func myLocationButtonTapped() {
        Analytics.logMyLocationButtonTappedEvent()
        if !mainView.moveCameraToCurrentLocation() {
            showLocationAccessDeniedError()
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func configureMap() {
        let map = mainView.googleMap
        
        map?.delegate = self
        map?.settings.rotateGestures = false
        map?.settings.tiltGestures = false
        map?.setMinZoom(6, maxZoom: 100)
    }
    
    internal func _removeRoute() {
        if let oldRoute = self.routePolyline {
            mainView.removeRouteLine(oldRoute)
        }
        
        mainView.removeMarker(destinationMarker)
        destinationMarker = nil
    }
    
    internal func removeAllParkingMarkers() {
        parkingMarkersContainer.markers().forEach({ mainView.removeMarker($0.marker) })
        parkingMarkersContainer.removeAllMarkers()
    }
    
    internal func removeSearchResultMarker() {
        mainView.removeMarker(searchResultMarker)
        searchResultMarker = nil
    }
    
    internal func showDestinationMarker(markerCoordinates: CLLocationCoordinate2D) {
        destinationMarker = GMSMarker(position: markerCoordinates)
        destinationMarker!.snippet = type(of: self).kDestinationMarkerSnippet
        destinationMarker!.icon = UIImage(named: "map_parking_slot_destination_marker.png")
        mainView.addMarker(destinationMarker)
    }
    
    internal func retrieveParkingSpacesForCurrentRegion() {
        let newRegion = mainView.googleMap.projection.visibleRegion()
        output.didChangeMapRegion(newRegion: newRegion)
    }
    
    
    
    // MARK: - Markers Appearance Management
    
    internal func updateSelectedMarkerInfoView() {
        let marker = mainView.googleMap.selectedMarker
        mainView.googleMap.selectedMarker = nil
        mainView.googleMap.selectedMarker = marker
    }
    
    internal func updateParkingMarkersIcon(markers: Set<ParkingMarker>) {
        markers.forEach{ $0.updateMarkerIcon(lastZoomLevel) }
    }
    
    internal func updateSelectedMarkersPosition() {
        parkingMarkersOverlaysContainer.overlays().forEach { actionView in
            let markerPosition = actionView.mapMarker.position
            let point = mainView.googleMap.projection.point(for: markerPosition)
            
            let centerX = point.x + (isSearchResultMarker(actionView.mapMarker) ? 8.5 : 16)
            let centerY = point.y - (isSearchResultMarker(actionView.mapMarker) ? 58 : 47.5)
            
            actionView.center = CGPoint(x: centerX, y: centerY)
        }
    }
    
    
    
    // MARK: - Selection Overlays
    
    internal func activateSelectionOverlays() {
        parkingMarkersOverlaysContainer.overlays().forEach({ $0.enableGoButton() })
    }
    
    internal func deactivateSelectionOverlays() {
        parkingMarkersOverlaysContainer.overlays().forEach({ $0.disableGoButton() })
    }
    
    internal func removeSelectionOverlays() {
        tappedMarkersContainer.markers().forEach({ unselectMarker($0.marker) })
        mainView.googleMap.selectedMarker = nil
    }
    
    
    
    // MARK: - Convenience Methods
    
    internal func parkingMarkerForMapMarker(_ marker: GMSMarker) -> ParkingMarker? {
        return parkingMarkersContainer.markers().filter { $0.marker == marker }.first
    }
    
    internal func isLocationVisible(location: CLLocationCoordinate2D) -> Bool {
        let region = mainView.googleMap.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        
        return bounds.contains(location)
    }
    
    internal func isDestinationMarker(_ marker: GMSMarker) -> Bool {
        let markerSnippet = marker.snippet ?? ""
        return markerSnippet == type(of: self).kDestinationMarkerSnippet
    }
    
    internal func isSearchResultMarker(_ marker: GMSMarker) -> Bool {
        let markerSnippet = marker.snippet ?? ""
        return markerSnippet == type(of: self).kSearchResultMarkerSnippet
    }
    
    private func locationAccessDeniedAlert() -> UIAlertController {
        let errorTitle = NSLocalizedString("MAP_LOCATION_NOT_AVAILABLE_ERROR_TITLE",
                                           comment: "Location not available error title")
        let errorMessage = NSLocalizedString("MAP_LOCATION_NOT_AVAILABLE_ERROR_MESSAGE",
                                             comment: "Location not available error message")
        let alertController = UIAlertController(title: errorTitle,
                                                message: errorMessage,
                                                preferredStyle: .alert)
        
        let openSettingsButtonTitle = NSLocalizedString("OPEN_SETTINGS_BUTTON_TITLE",
                                                        comment: "Open Settings button title")
        let openSettingsAction = UIAlertAction(title: openSettingsButtonTitle, style: .default) { _ in
            if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
        alertController.addAction(openSettingsAction)
        
        let cancelButtonTitle = NSLocalizedString("CANCEL_BUTTON_TITLE",
                                                  comment: "Cancel button title")
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
}
