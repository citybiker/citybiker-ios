//
//  SearchResultMarker.swift
//  Citybiker
//
//  Created by Vlad on 9/9/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import GoogleMaps

class SearchResultMarker: GMSMarker {
    
    init(position: CLLocationCoordinate2D) {
        super.init()
        
        super.position = position
        icon = UIImage(named: "map_search_selected_result_marker")
        appearAnimation = kGMSMarkerAnimationPop
        groundAnchor = CGPoint(x: 0.5, y: 1)
    }
    
}
