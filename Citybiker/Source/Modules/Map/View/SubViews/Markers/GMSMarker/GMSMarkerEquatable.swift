//
//  GMSMarkerEquatable.swift
//  Citybiker
//
//  Created by Vlad on 10/20/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import GoogleMaps

public func ==(lhs: GMSMarker, rhs: GMSMarker) -> Bool {
    return lhs.position == rhs.position
}

public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return lhs.longitude == rhs.longitude
        && lhs.latitude == rhs.latitude
}
