//
//  ParkingMarkerInfo.swift
//  Citybiker
//
//  Created by Vlad on 9/7/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class ParkingMarkerInfoView: PassthroughView {
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var titleLabelBackground: UIImageView!
    
    
    // MARK: - Public Methods
    
    func set(numberOfSpaces spaces: Int) {
        let numberOfSpacesString = String(spaces)
        let spacesString = NSLocalizedString("MAP_MARKER_INFO_SPACES",
                                             comment: "Maker spaces string")
        let titleString = numberOfSpacesString + " " + spacesString
        let titleAttributedString = NSMutableAttributedString(string: titleString)
        
        titleAttributedString.addAttribute(NSFontAttributeName,
                                           value: UIFont(name: "Montserrat-Bold", size: 9.5)!,
                                           range: NSMakeRange(0, numberOfSpacesString.characters.count))
        titleAttributedString.addAttribute(NSFontAttributeName,
                                           value: UIFont(name: "Montserrat-Regular", size: 8.5)!,
                                           range: NSMakeRange(numberOfSpacesString.characters.count + 1,
                                            spacesString.characters.count))
        
        self.titleLabel.attributedText = titleAttributedString
    }
    
    func hideNumberOfSpaces() {
        titleLabel.isHidden = true
        titleLabelBackground.isHidden = true
    }
    
}
