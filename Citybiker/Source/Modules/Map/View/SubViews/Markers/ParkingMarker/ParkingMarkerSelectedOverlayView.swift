//
//  ParkingMarkerActionView.swift
//  Citybiker
//
//  Created by Vlad on 9/8/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import GoogleMaps

class ParkingMarkerSelectedOverlayView: PassthroughView {
    
    @IBOutlet private weak var goButton: UIButton!
    @IBOutlet private weak var parkingSign: UIImageView?
    
    weak var mapMarker: GMSMarker!
    var goButtonAction: (() -> ())?
    
    
    // MARK: - Public Methods
    
    func enableGoButton() {
        goButton.isEnabled = true
    }
    
    func disableGoButton() {
        goButton.isEnabled = false
    }
    
    func showAnimated(_ completion: (() -> ())? = nil) {
        if parkingSign != nil {
            showParkingSign({ [weak self] in
                self?.showGoButton({
                    completion?()
                })
            })
        } else {
            showGoButton({
                completion?()
            })
        }
    }
    
    func hideAnimated(_ completion: (() -> ())? = nil) {
        if parkingSign != nil {
            hideGoButton({ [weak self] in
                self?.hideParkingSign({ 
                    completion?()
                })
            })
        } else {
            hideGoButton({ 
                completion?()
            })
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func showParkingSign(_ completion: (() -> ())? = nil) {
        UIView.animate(withDuration: 0.2, animations: {
            let scaleTransform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            let scaleAndTranslateTransform = scaleTransform.translatedBy(x: 0, y: -6)
            self.parkingSign!.transform = scaleAndTranslateTransform
        }) { _ in
            completion?()
        }
    }
    
    private func hideParkingSign(_ completion: (() -> ())? = nil) {
        UIView.animate(withDuration: 0.2, animations: {
            self.parkingSign!.transform = CGAffineTransform.identity
        }) { _ in
            completion?()
        }
    }
    
    private func showGoButton(_ completion: (() -> ())? = nil) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.goButton.alpha = 1
        }) { _ in
            completion?()
        }
    }
    
    private func hideGoButton(_ completion: (() -> ())? = nil) {
        UIView.animate(withDuration: 0.2, animations: {
            self.goButton.alpha = 0
        }) { _ in
            completion?()
        }
    }
    
    
    
    // MARK: - Action
    
    @IBAction internal func goButtonTapped() {
        goButtonAction?()
    }
    
}
