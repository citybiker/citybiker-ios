//
//  ParkingMarker.swift
//  Citybiker
//
//  Created by Vlad on 8/6/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import GoogleMaps

open class ParkingMarker {
    
    let id: Int
    let marker: GMSMarker
    
    init(id: Int, position: CLLocationCoordinate2D, numberOfSpaces: Int) {
        self.id = id
        
        marker = GMSMarker(position: position)
        marker.snippet = String(numberOfSpaces)
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.infoWindowAnchor = CGPoint(x: 0.5, y: 1.42)
    }
    
    func makeTransparent() {
        marker.icon = transparentImage()
        marker.groundAnchor = markerGroundAnchorForZoomLevel(.streetLevel)
    }
    
    func reset(_ zoomLevel: ZoomLevel) {
        updateMarkerIcon(zoomLevel)
    }
    
    func updateMarkerIcon(_ zoomLevel: ZoomLevel) {
        marker.icon = markerImageForZoomLevel(zoomLevel)
        marker.groundAnchor = markerGroundAnchorForZoomLevel(zoomLevel)
    }
    
    
    
    // MARK: - Private Methods
    
    private func transparentImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 32, height: 39), false, 0)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    private func markerImageForZoomLevel(_ zoomLevel: ZoomLevel) -> UIImage {
        switch zoomLevel {
        case .streetLevel:
            return UIImage(named: "map_parking_slot_marker")!
        case .districtLevel:
            return UIImage(named: "map_big_point_marker")!
        case .cityLevel:
            return UIImage(named: "map_middle_point_marker")!
        case .countryLevel:
            return UIImage(named: "map_small_point_marker")!
        }
    }
    
    private func markerGroundAnchorForZoomLevel(_ zoomLevel: ZoomLevel) -> CGPoint {
        switch zoomLevel {
        case .streetLevel:
            return CGPoint(x: 0.5, y: 1)
        default:
            return CGPoint(x: 0.5, y: 0.5)
        }
    }
    
}


public func ==(lhs: ParkingMarker, rhs: ParkingMarker) -> Bool {
    return lhs.id == rhs.id
}

extension ParkingMarker: Hashable {
    public var hashValue: Int {
        return "\(id))".hashValue
    }
}
