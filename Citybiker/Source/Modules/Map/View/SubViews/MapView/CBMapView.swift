//
//  CBMapView.swift
//  Citybiker
//
//  Created by Vlad on 8/3/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import GoogleMaps

class MapView: UIView {
    
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var routeDetailsContainerView: UIView!
    
    
    // MARK: - Public Methods
    
    func showMyLocationDot(show: Bool) {
        googleMap.isMyLocationEnabled = show
    }
    
    func showCurrentLocationButton(show: Bool) {
        googleMap.settings.myLocationButton = show
    }
    
    func moveCameraToCurrentLocation() -> Bool {
        if let location = googleMap.myLocation {
            animateCameraMovesAndZoomToSeeLocation(location.coordinate)
            return true
        }
        
        return false
    }
    
    func moveCameraToLocation(_ location: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withTarget: location, zoom: 15)
        googleMap.camera = camera
    }
    
    func animateCameraMovesAndZoomToSeeLocation(_ location: CLLocationCoordinate2D) {
        googleMap.animate(toLocation: location)
        googleMap.animate(toZoom: 17)
    }
    
    func addMarker(_ marker: GMSMarker?) {
        marker?.map = googleMap
    }
    
    func removeMarker(_ marker: GMSMarker?) {
        marker?.map = nil
    }
    
    func showRouteLine(_ route: Route) -> RoutePolyline {
        let path = route.segments != nil ? pathFromRouteSegments(route.segments!) : GMSMutablePath()
        let routePolyline = RoutePolyline(path: path)
        routePolyline.addToMap(googleMap)
        
        return routePolyline
    }
    
    func fitRoutePath(_ path: GMSPath, with edgeInsets: UIEdgeInsets) {
        let cameraUpdate = GMSCameraUpdate.fit(GMSCoordinateBounds(path: path), with: edgeInsets)
        googleMap.animate(with: cameraUpdate)
    }
    
    func removeRouteLine(_ routePolyline: RoutePolyline) {
        routePolyline.removeFromMap()
    }
    
    
    
    // MARK: - Private Methods
    
    func pathFromRouteSegments(_ segments: Array<RouteSegment>) -> GMSMutablePath {
        let path = GMSMutablePath()
        for segment in segments {
            if let allCoordinates = segment.coordinates {
                for coordinate in allCoordinates {
                    path.add(coordinate)
                }
            }
        }
        
        return path
    }
    
}
