//
//  MapZoomLevel.swift
//  Citybiker
//
//  Created by Vlad on 10/20/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

enum ZoomLevel: Float {
    case streetLevel = 21
    case districtLevel = 14
    case cityLevel = 13
    case countryLevel = 11
    
    init(zoom: Float) {
        if zoom < 10 {
            self = .countryLevel
        } else if zoom < 13 {
            self = .cityLevel
        } else if zoom < 14 {
            self = .districtLevel
        } else {
            self = .streetLevel
        }
    }
}
