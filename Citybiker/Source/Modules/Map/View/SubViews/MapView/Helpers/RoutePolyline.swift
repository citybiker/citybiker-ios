//
//  RoutePolyline.swift
//  Citybiker
//
//  Created by Vlad on 9/9/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import GoogleMaps

class RoutePolyline {
    
    let path: GMSPath
    private let polylineBackground: GMSPolyline
    private let polylineForeground: GMSPolyline
    
    init(path: GMSPath) {
        self.path = path
        
        polylineBackground = GMSPolyline(path: path)
        polylineBackground.strokeWidth = 10
        polylineBackground.strokeColor = UIColor.cbLightGrayColor()
        
        polylineForeground = GMSPolyline(path: path)
        polylineForeground.strokeWidth = 6
        polylineForeground.strokeColor = UIColor.cbOrangeColor()
    }
    
    func addToMap(_ map: GMSMapView) {
        polylineBackground.map = map
        polylineForeground.map = map
    }
    
    func removeFromMap() {
        polylineBackground.map = nil
        polylineForeground.map = nil
    }
    
}
