//
//  CBMapViewController+GMSMapViewDelegate.swift
//  Citybiker
//
//  Created by Vlad on 10/19/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import GoogleMaps

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if destinationMarker == nil {
            self.retrieveParkingSpacesForCurrentRegion()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let currentZoomLevel = ZoomLevel(zoom: position.zoom)
        if lastZoomLevel != currentZoomLevel {
            lastZoomLevel = currentZoomLevel
            
            updateParkingMarkersIcon(markers: parkingMarkersContainer.markers())
        }
        
        updateSelectedMarkersPosition()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.animate(toLocation: focusCoordinate(forMarker: marker))
        
        if isDestinationMarker(marker) {
            return true
        } else {
            mapView.selectedMarker = marker
            
            if !isSearchResultMarker(marker) {
                parkingMarkerForMapMarker(marker)?.makeTransparent()
            }
            selectMarker(marker)
            
            return true
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        guard let snippet = marker.snippet, let numberOfSpaces = Int(snippet) else {
            return markerInfoView(numberOfSpaces: 0)
        }
        
        return markerInfoView(numberOfSpaces: numberOfSpaces)
    }
    
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        unselectMarker(marker)
    }
    
    
    
    // MARK: - Private Methods
    
    private func focusCoordinate(forMarker marker: GMSMarker) -> CLLocationCoordinate2D {
        var focusPoint = mainView.googleMap.projection.point(for: marker.position)
        focusPoint.y -= 100
        
        return mainView.googleMap.projection.coordinate(for: focusPoint)
    }
    
    private func markerInfoView(numberOfSpaces: Int) -> ParkingMarkerInfoView {
        let markerInfoView = Bundle.main.loadNibNamed("ParkingMarkerInfoView", owner: self, options: nil)?[0] as! ParkingMarkerInfoView
        markerInfoView.set(numberOfSpaces: numberOfSpaces)
        if (numberOfSpaces == 0) {
            markerInfoView.hideNumberOfSpaces()
        }
        
        return markerInfoView
    }
    
    internal func selectMarker(_ marker: GMSMarker) {
        if let tappedParkingMarker = (parkingMarkersContainer.markers().filter { $0.marker == marker }.first) {
            parkingMarkersContainer.removeMarker(marker: tappedParkingMarker)
            tappedMarkersContainer.addMarker(marker: tappedParkingMarker)
        }
        
        let selectionOverlay = markerSelectionOverlay(forMaker: marker)
        selectionOverlay.mapMarker = marker
        
        mainView.googleMap.addSubview(selectionOverlay)
        selectionOverlay.showAnimated()
        
        parkingMarkersOverlaysContainer.addOverlay(overlay: selectionOverlay)
    }
    
    internal func unselectMarker(_ marker: GMSMarker) {
        if let tappedParkingMarker = (tappedMarkersContainer.markers().filter { $0.marker == marker }.first) {
            tappedMarkersContainer.removeMarker(marker: tappedParkingMarker)
            parkingMarkersContainer.addMarker(marker: tappedParkingMarker)
        }
        
        parkingMarkersOverlaysContainer.overlays().filter { $0.mapMarker == marker }.forEach { markerOverlayView in
            markerOverlayView.hideAnimated(
                {
                    self.parkingMarkerForMapMarker(marker)?.reset(self.lastZoomLevel)
                    
                    let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.01 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute:
                        { [weak self] in
                            markerOverlayView.removeFromSuperview()
                            self?.parkingMarkersOverlaysContainer.removeOverlay(overlay: markerOverlayView)
                        })
                })
        }
    }
    
    
    
    // MARK: - Selection Overlay
    
    private func markerSelectionOverlay(forMaker marker: GMSMarker) -> ParkingMarkerSelectedOverlayView {
        let markerSelectionOverlay = isSearchResultMarker(marker) ?
        Bundle.main.loadNibNamed("SearchResultSelectedOverlayView", owner: self, options: nil)?[0] as! ParkingMarkerSelectedOverlayView :
        Bundle.main.loadNibNamed("ParkingMarkerSelectedOverlayView", owner: self, options: nil)?[0] as! ParkingMarkerSelectedOverlayView
        
        markerSelectionOverlay.center = selectionOverlayViewPosition(marker: marker)
        markerSelectionOverlay.goButtonAction = { [weak self] in
            self?.deactivateSelectionOverlays()
            self?.output.didSelectDestinationLocation(destinationLocation: marker.position)
        }
        
        return markerSelectionOverlay
    }
    
    private func selectionOverlayViewPosition(marker: GMSMarker) -> CGPoint {
        let markerPosition = marker.position
        let point = mainView.googleMap.projection.point(for: markerPosition)
        let pointX = point.x + (isSearchResultMarker(marker) ? 8.5 : 16)
        let pointY = point.y - (isSearchResultMarker(marker) ? 58 : 47.5)
        
        return CGPoint(x: pointX, y: pointY)
    }
    
}
