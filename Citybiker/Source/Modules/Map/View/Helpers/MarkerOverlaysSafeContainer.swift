//
//  MarkerOverlaysSafeContainer.swift
//  Citybiker
//
//  Created by Vlad on 10/20/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

class MarkerOverlaysSafeContainer {
    
    private var allOverlays = [ParkingMarkerSelectedOverlayView]()
    
    
    // MARK: - Public Methods
    
    func overlays() -> [ParkingMarkerSelectedOverlayView] {
        objc_sync_enter(allOverlays)
        defer { objc_sync_exit(allOverlays) }
        
        return allOverlays
    }
    
    func addOverlay(overlay: ParkingMarkerSelectedOverlayView) {
        objc_sync_enter(allOverlays)
        defer { objc_sync_exit(allOverlays) }
        
        allOverlays.append(overlay)
    }
    
    func removeOverlay(overlay: ParkingMarkerSelectedOverlayView) {
        objc_sync_enter(allOverlays)
        defer { objc_sync_exit(allOverlays) }
        
        allOverlays.remove(object: overlay)
    }
    
    func removeAllOverlays() {
        objc_sync_enter(allOverlays)
        defer { objc_sync_exit(allOverlays) }
        
        allOverlays.removeAll()
    }
    
}
