//
//  MarkersSafeContainer.swift
//  Citybiker
//
//  Created by Vlad on 10/20/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation

class MarkersSafeContainer<Marker:Hashable> {
    
    private var allMarkers = Set<Marker>()
    
    
    // MARK: - Public Methods
    
    func markers() -> Set<Marker> {
        objc_sync_enter(allMarkers)
        defer { objc_sync_exit(allMarkers) }
        
        return allMarkers
    }
    
    func addMarker(marker: Marker) {
        objc_sync_enter(allMarkers)
        defer { objc_sync_exit(allMarkers) }
        
        let markers = Set<Marker>(arrayLiteral: marker)
        allMarkers.formUnion(markers)
    }
    
    func addMarkers(markers: Set<Marker>) {
        objc_sync_enter(allMarkers)
        defer { objc_sync_exit(allMarkers) }
        
        allMarkers.formUnion(markers)
    }
    
    func removeMarker(marker: Marker) {
        objc_sync_enter(allMarkers)
        defer { objc_sync_exit(allMarkers) }
        
        let markers = Set<Marker>(arrayLiteral: marker)
        allMarkers.subtract(markers)
    }
    
    func removeMarkers(markers: Set<Marker>) {
        objc_sync_enter(allMarkers)
        defer { objc_sync_exit(allMarkers) }
        
        allMarkers.subtract(markers)
    }
    
    func removeAllMarkers() {
        objc_sync_enter(allMarkers)
        defer { objc_sync_exit(allMarkers) }
        
        allMarkers.removeAll()
    }
    
}
