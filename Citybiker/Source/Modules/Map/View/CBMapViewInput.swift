//
//  MapCBMapViewInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import MapKit
import GoogleMaps

protocol MapViewInput: class {
    
    var searchContainer: UIView { get }
    var routeDetailsContainer: UIView { get }
    var currentLocation: CLLocationCoordinate2D? { get }
    
    
    func setupInitialState()
    
    func showMyLocationDot()
    func showLocationAccessDeniedError()
    
    func moveMapCameraToLocation(_ location: CLLocationCoordinate2D)
    func moveMapCameraAndShowSearchResultMarker(for mapItem: MKMapItem)
    
    func showParkingMarkers(_ parkingMarkersInCurrentRegion: Set<ParkingMarker>)
    
    func showRoute(_ route: Route, zoomToFit: Bool)
    func removeRoute()
    
    func showError(errorMessage: String)
    
}
