//
//  MapCBMapRouterInput.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

protocol MapRouterInput {
    
    func showSearchScreen(onParentController parentController: UIViewController, containerView: UIView) -> SearchModuleInput
    func showRouteDetailsScreen(onParentController parentController: UIViewController, containerView: UIView) -> RouteDetailsModuleInput
    
    func hideScreen(screenController: UIViewController)
    
}
