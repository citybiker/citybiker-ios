//
//  MapCBMapRouter.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class MapRouter: MapRouterInput {
    
    func showSearchScreen(onParentController parentController: UIViewController, containerView: UIView) -> SearchModuleInput {
        let searchScreen = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController() as! SearchViewController
        
        parentController.addChildViewController(searchScreen)
        searchScreen.view.frame = containerView.bounds
        containerView.addSubview(searchScreen.view)
        searchScreen.didMove(toParentViewController: parentController)
        
        return searchScreen.output as! SearchModuleInput
    }
    
    func showRouteDetailsScreen(onParentController parentController: UIViewController, containerView: UIView) -> RouteDetailsModuleInput {
        let routeDetailsScreen = UIStoryboard(name: "RouteDetails", bundle: nil).instantiateInitialViewController() as! RouteDetailsViewController
        
        parentController.addChildViewController(routeDetailsScreen)
        routeDetailsScreen.view.frame = containerView.bounds
        containerView.addSubview(routeDetailsScreen.view)
        routeDetailsScreen.didMove(toParentViewController: parentController)
        
        return routeDetailsScreen.output as! RouteDetailsModuleInput
    }
    
    func hideScreen(screenController: UIViewController) {
        screenController.willMove(toParentViewController: nil)
        screenController.removeFromParentViewController()
        screenController.view.removeFromSuperview()
    }
    
}
