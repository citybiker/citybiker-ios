//
//  CBMapCBMapConfigurator.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class MapModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? MapViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: MapViewController) {
        let router = MapRouter()
        
        let presenter = MapPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = MapInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
