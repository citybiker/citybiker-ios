//
//  CBMapCBMapInitializer.swift
//  Citybiker
//
//  Created by Vlad Ionita on 03/08/2016.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit

class MapModuleInitializer: NSObject {
    
    @IBOutlet weak var mapViewController: MapViewController!
    
    override func awakeFromNib() {
        let configurator = MapModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: mapViewController)
    }
    
}
