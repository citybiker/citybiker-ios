//
//  FirebaseAnalyticsService.swift
//  Citybiker
//
//  Created by Vlad on 10/9/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import Firebase

class FirebaseAnalyticsService: NSObject, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
        [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        FIRApp.configure()
        
        return true
    }
    
}
