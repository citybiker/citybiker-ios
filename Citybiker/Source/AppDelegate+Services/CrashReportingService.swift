//
//  CrashReportingService.swift
//  WeTune
//
//  Created by Vlad on 9/22/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

class CrashReportingService: NSObject, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        Fabric.with([Crashlytics.self])
        
        return true
    }
    
}
