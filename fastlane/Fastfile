# Customise this file, documentation can be found here:
# https://github.com/fastlane/fastlane/tree/master/fastlane/docs
# All available actions: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Actions.md
# can also be listed using the `fastlane actions` command

# Change the syntax highlighting to Ruby
# All lines starting with a # are ignored when running `fastlane`

# If you want to automatically update fastlane if a new version is available:
# update_fastlane

# This is the minimum version number required.
# Update this, if you use features of a newer version
fastlane_version "1.106.2"

default_platform :ios

platform :ios do

	### Public lanes

	desc "Sync all certificates"
	lane :match_sync do
		match(
			app_identifier: "dev.citybiker.ios",
			type: "development",
			force_for_new_devices: true,
		)
		match(
			app_identifier: "beta.citybiker.ios",
			type: "adhoc",
			force_for_new_devices: true,
		)
		match(
			app_identifier: "com.citybiker.ios",
			type: "adhoc",
			force_for_new_devices: true,
		)
		match(
			app_identifier: "com.citybiker.ios",
			type: "appstore",
		)
	end

	desc "Submit a beta build to crashlytics"
	lane :crashlytics_beta do
		ensure_git_status_clean
		ensure_git_branch(
			branch: 'develop'
		)

		changelog_title = "Changelog: \n"
		changelog_input = prompt(
			text: "Enter the change log:",
			multi_line_end_keyword: "END",
		)
		changelog =  changelog_title + changelog_input
		
		increment_build_number

		version_number = "#{get_version_number}"
		build_number = "#{get_build_number}"
		version_string = version_number + "." + build_number

		commit_version_bump(message: "- version bump, build #{build_number}", force: true)

		badge(
			# shield: "Version-#{version_string}-orange",
			# shield_no_resize: true,
			glob: "/Citybiker/**/*.appiconset/*.{png,PNG}",
		)

		sigh(
			adhoc: true,
			force: true,
		)

		ipa_name = "Citybiker-#{version_string}.ipa"
		gym(
			use_legacy_build_api: true,
			workspace: "Citybiker.xcworkspace",
			configuration: "Beta",
			scheme: "Citybiker",
			silent: true,
			clean: true,
			output_directory: "./CrashlyticsBetaBuilds", # Destination directory. Defaults to current directory.
			output_name: ipa_name, # specify the name of the .ipa file to generate (including file extension)
		)

		crashlytics(
			crashlytics_path: 'Pods/Crashlytics/iOS/Crashlytics.framework', # path to your 'Crashlytics.framework'
			api_token: '9e1fc64018b7b435c3e05f630d6ad39f2aa46d9b',
			build_secret: 'bda3d67d5e1851a2309df7cb74468bd9d8ba1d3a62d20b154ca16c8c872da3d4',
			ipa_path: "./CrashlyticsBetaBuilds/#{ipa_name}",
			notes: changelog,
			groups: ["citybiker-internal"],
		)

		clean_build_artifacts

		slack(
			message: "Beta version #{version_string} successfully released! :rocket:",
			default_payloads: [], # reduce the notification to the minimum
			payload: {
				"Configuration" => "Beta",
				"Changes" => changelog,
			}
		)

		git_tag = "v" + version_string
		add_git_tag(
			tag: git_tag
		)

		push_to_git_remote(
			remote: 'origin',
			local_branch: 'develop',
			remote_branch: 'develop',
			force: false
		)

		reset_git_repo(
			force: true
		)
	end


	desc "Update the last beta build on crashlytics"
	lane :crashlytics_beta_update do
		ensure_git_status_clean
		
		ensure_git_branch(
			branch: 'develop'
		)

		version_number = "#{get_version_number}"
		build_number = "#{get_build_number}"
		version_string = version_number + "." + build_number

		badge(
			# shield: "Version-#{version_string}-orange",
			# shield_no_resize: true,
			glob: "/Citybiker/**/*.appiconset/*.{png,PNG}",
		)

		sigh(
			adhoc: true,
			force: true,
		)

		ipa_name = "Citybiker-#{version_string}.ipa"
		gym(
			use_legacy_build_api: true,
			workspace: "Citybiker.xcworkspace",
			configuration: "Beta",
			scheme: "Citybiker",
			silent: true,
			clean: true,
			output_directory: "./CrashlyticsBetaBuilds", # Destination directory. Defaults to current directory.
			output_name: ipa_name, # specify the name of the .ipa file to generate (including file extension)
		)

		crashlytics(
			crashlytics_path: 'Pods/Crashlytics/iOS/Crashlytics.framework', # path to your 'Crashlytics.framework'
			api_token: '9e1fc64018b7b435c3e05f630d6ad39f2aa46d9b',
			build_secret: 'bda3d67d5e1851a2309df7cb74468bd9d8ba1d3a62d20b154ca16c8c872da3d4',
			ipa_path: "./CrashlyticsBetaBuilds/#{ipa_name}",
		)

		clean_build_artifacts

		slack(
			message: "Beta version #{version_string} successfully updated! :rocket:",
			default_payloads: [], # reduce the notification to the minimum
		)

		reset_git_repo(
			force: true
		)
	end

	desc "Submit a store build to crashlytics"
	lane :crashlytics_store do
		ensure_git_status_clean
		
		ensure_git_branch

		version_number = "#{get_version_number}"
		build_number = "#{get_build_number}"
		version_string = version_number + "." + build_number

		changelog_additional_info = "Store version #{version_number}\n"
		changelog_title = "Changelog: \n"
		changelog_input = File.read('metadata/en-US/release_notes.txt')
		changelog = changelog_additional_info + changelog_title + changelog_input
		
		sigh(
			adhoc: true,
			force: true,
		)

		ipa_name = "Citybiker-#{version_string}.ipa"
		gym(
			use_legacy_build_api: true,
			workspace: "Citybiker.xcworkspace",
			configuration: "Release",
			scheme: "Citybiker",
			silent: true,
			clean: true,
			output_directory: "./CrashlyticsReleaseBuilds", # Destination directory. Defaults to current directory.
			output_name: ipa_name, # specify the name of the .ipa file to generate (including file extension)
		)

		crashlytics(
			crashlytics_path: 'Pods/Crashlytics/iOS/Crashlytics.framework', # path to your 'Crashlytics.framework'
			api_token: '9e1fc64018b7b435c3e05f630d6ad39f2aa46d9b',
			build_secret: 'bda3d67d5e1851a2309df7cb74468bd9d8ba1d3a62d20b154ca16c8c872da3d4',
			ipa_path: "./CrashlyticsReleaseBuilds/#{ipa_name}",
			notes: changelog,
			groups: ["citybiker-internal"],
		)

		clean_build_artifacts

		slack(
			message: "Store version #{version_number} successfully released! :rocket:",
			default_payloads: [], # reduce the notification to the minimum
			payload: {
				"Configuration" => "Store",
				"Changes" => changelog,
			}
		)

		push_to_git_remote(
			remote: 'origin',
			local_branch: 'master',
			remote_branch: 'master',
			force: false
		)

		reset_git_repo(
			force: true
		)
	end


	desc "Deploy to AppStore"
	lane :deploy do |options|
		prepare_for_deploy(version_bump: options[:version_bump])
		deploy_continue
	end


	desc "Deploy to AppStore fall-back"
	lane :deploy_continue do
		sigh(
			force: true,
		)

		ipa_name = "Citybiker-#{version_string}.ipa"
		gym(
			use_legacy_build_api: true,
			workspace: "Citybiker.xcworkspace",
			configuration: "Release",
			scheme: "Citybiker",
			silent: true,
			clean: true,
			output_directory: "./CrashlyticsReleaseBuilds", # Destination directory. Defaults to current directory.
			output_name: ipa_name, # specify the name of the .ipa file to generate (including file extension)
		)

		deliver(
			force: true
		)

		clean_build_artifacts

		slack(
			message: "Store version #{version_number} successfully released! :green_apple: :pray:",
			default_payloads: [], # reduce the notification to the minimum
			payload: {
				"Configuration" => "Store",
				"Changes" => changelog,
			}
		)

		push_to_git_remote(
			remote: 'origin',
			local_branch: 'master',
			remote_branch: 'master',
		)

		reset_git_repo(
			force: true
		)
	end



	### Private lanes

	desc "Preparation for deploy"
	private_lane :prepare_for_deploy do |options|
		ensure_git_status_clean

		ensure_git_branch(
			branch: 'develop'
		)

		version_bump = (options[:version_bump] ? options[:version_bump] : "patch")
		version_number = increment_version_number(
			bump_type: "#{version_bump}",
		)
		build_number = "#{get_build_number}"
		version_string = version_number + "." + build_number

		release_branch_name = "release-#{version_number}"
		sh "git checkout -B #{release_branch_name}"

		update_release_notes
		update_screenshots

		commit_version_bump(message: "- version bump, release #{version_number}", force: true)

		git_tag = "v" + version_number
		add_git_tag(
			tag: git_tag
		)

		sh "git checkout develop"
		sh "git merge --no-ff #{release_branch_name}"

		sh "git checkout master"
		sh "git merge --no-ff #{release_branch_name}"
	end


	desc "Update release note"
	private_lane :update_release_notes do
		changelog = prompt(
			text: "Enter the change log:",
			multi_line_end_keyword: "END",
		)

		File.write('metadata/en-US/release_notes.txt', changelog)
		sh "git add metadata/en-US/release_notes.txt"
		sh "git commit -m \"- Fastlane: release notes updated\" || true"
	end


	desc "Update the screenshots so that they have a perfect status bar"
	private_lane :update_screenshots do
		require 'io/console'

		puts "Make sure you have the new screenshots in the fastlane/resources folder.\nWhen ready, press enter to continue"
		STDIN.getch

		status_bar_path = "./resources/status_bar@3x.png"
		for i in 2..5
			source_image_file_path = "./resources/#{i}_iphone6Plus_#{i}.screenshot_#{i}.png"
			output_image_file_path = "./screenshots/en-US/#{i}_iphone6Plus_#{i}.screenshot_#{i}.png"

			sh "composite #{status_bar_path} #{source_image_file_path} #{output_image_file_path}"

			# sleep(1)
			File.delete("#{source_image_file_path}")
		end

		sh "git add screenshots/en-US/"
		sh "git commit -m \"- Fastlane: new app store images\" || true"
	end



	### Fastlane special

	before_all do
		ENV["SLACK_URL"] = "https://hooks.slack.com/services/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"
	end


	after_all do |lane|
		# This block is called, only if the executed lane was successful
	end


	error do |lane, exception|
		slack(
			message: exception.message,
			success: false
		)
	end

end


# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Actions.md

# fastlane reports which actions are used
# No personal data is recorded. Learn more at https://github.com/fastlane/enhancer
