fastlane documentation
================
# Installation
```
sudo gem install fastlane
```
# Available Actions
## iOS
### ios match_sync
```
fastlane ios match_sync
```
Sync all certificates
### ios crashlytics_beta
```
fastlane ios crashlytics_beta
```
Submit a beta build to crashlytics
### ios crashlytics_beta_update
```
fastlane ios crashlytics_beta_update
```
Update the last beta build on crashlytics
### ios crashlytics_store
```
fastlane ios crashlytics_store
```
Submit a store build to crashlytics
### ios deploy
```
fastlane ios deploy
```
Deploy to AppStore
### ios deploy_continue
```
fastlane ios deploy_continue
```
Deploy to AppStore fall-back

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [https://fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [GitHub](https://github.com/fastlane/fastlane/tree/master/fastlane).
